using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class NPCMinion : NPC {
	
	[SerializeField]
	int cooldownTime = 5;
	
	int _respawnTime;
	
	int _oldType;
	
	bool _roaming;
	
	
	protected override void Init()
	{
		_diagOverride = false;
		LateInit();
	}
	
	//add the zones where the npc will move
	protected override void LateInit()
	{
		_roaming = true;
		switch(id)
		{
		case "ditem_butler":
		case "ditem_maid":
			_roamingZones.AddRange(new Zone.EZONETYPE[]{Zone.EZONETYPE.BASEMENT, Zone.EZONETYPE.FIRSTFLOOR, Zone.EZONETYPE.SECONDFLOOR});
			break;
		case "ditem_gardener":
			_roamingZones.AddRange(new Zone.EZONETYPE[]{ Zone.EZONETYPE.GARDEN, Zone.EZONETYPE.CRYPT});
			break;
		default:
			_roaming = false;
			break;
		}
	}
	
	//set the npc properties
	public override void SetProperties(string id, CharacterManager.ENPC t, Division d)
	{
		base.SetProperties(id, t, d);
		transform.name = id;
		if(ShouldAppear())
		{
			_showing = true;
			_token.enabled = true;
			Color cc = _token.color;
			cc.a = 1f;
			LeanTween.color(GetComponent< RectTransform >(), cc, 0.25f);
			_firstAppear = false;
			
			//rearrange the tokens visible in the current division
			_currLoc.RearrangeTokens();
		}
	}
	
	//get this npc dialogue id
	public override string GetDItemID()
	{
		return "ditem_" + _type.ToString().ToLower();
	}
	
	//get this npc dialogue
	public override Dialogue GetDialogue()
	{
		return HoDManager.instance.dialogueManager.dialogueItems[GetDItemID()];
	}
	
	//condition to make the npc reappear in the map
	protected override bool ShouldAppear()
	{
		return _currLoc.GetVisibleChars().Count > 0;
	}
	
	//choose a new division for this npc
	protected override Division ChooseNewLoc (bool useAdjacent)
	{
		if(_currLoc != null)
		{
			List< Division > adj = _currLoc.adjacent;
			List< Division> divsWithChar = new List<Division>();
			foreach(Division d in adj)
			{
				if(d.GetVisibleChars().Count > 0)
				{
					divsWithChar.Add(d);
				}
			}
			if(divsWithChar.Count > 0)
			{
				_hasCaught = true;
				return divsWithChar[Random.Range(0, divsWithChar.Count)];
			}
		}
		
		return base.ChooseNewLoc(useAdjacent);
	}
	
	
	
	protected override void DisappearAndWait(int turns)
	{
		Teleport(false);
		_hasCaught = false;
		_respawnTime = turns;
	}
	
	//execute an action
	protected override void DoAction()
	{
		int avgS = HoDManager.instance.charManager.GetAvgSanity();
		
		//if the npc is not a cultist, check if the conditions to transform it apply
		if(_type != CharacterManager.ENPC.ROAMING_CULTIST)
		{
			if(avgS <= 50)
			{	//transform the npc into a cultist
				_oldType = (int)_type;
				_type = CharacterManager.ENPC.ROAMING_CULTIST;
				_respawnTime = 1;
			}
			else if(!_roaming)
			{	//end npc turn
				FinishRoam();
				return;
			}
		}
		else if(avgS > 50)
		{	//transform the cultist into its old npc type
			_type = (CharacterManager.ENPC)_oldType;
			FinishRoam();
			return;
		}
		
		float rnd = Random.Range(0f,1f);
		
		if(_respawnTime > 0)
		{	//npc is invisible, waiting to respawn
			--_respawnTime;
			FinishRoam();
		}
		else if(_hasCaught)
		{	//if the npc has caught a player, make it disappear
			DisappearAndWait(cooldownTime);
		}
		else if( rnd > 0.5f || HasCharAdjacent())
		{	//move the npc
			MoveToAdjacentLoc();
		}
		else
		{	//end turn
			FinishRoam();
		}
	}
	
	//get npc save string with its properties
	public override string GetSaveStr()
	{
		string res = base.GetSaveStr() +";"+_oldType;
		return res;
	}
	
	//load npc properties from a string
	public override void Load(string s)
	{
		string[] split = s.Split(';');
		base.Load(s);
		
		_oldType = int.Parse(split[5]);
	}
}
