using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CosmicHorror : NPC {
	
	[SerializeField]
	int cooldownTime = 3;
	
	int _respawnTime;
	
	void Awake()
	{
	}
	
	protected override void Init()
	{
		isRegularNPC = false;
		_hasCaught = false;
		_token.enabled = false;
		_diagOverride = true;
		LateInit();
	}
	
	//set the roaming zones for the cosmic horror
	protected override void LateInit ()
	{
		_roamingZones = new List<Zone.EZONETYPE>();
		if(id == "ch1")
		{
			AddRoaming(Zone.EZONETYPE.BASEMENT);
		}
		else
		{
			AddRoaming(Zone.EZONETYPE.CRYPT);
		}
	}
	
	//get the cosmic horror dialogue id
	public override string GetDItemID(){ return "ditem_horror"; }
	
	//get the cosmic horror dialogue
	public override Dialogue GetDialogue()
	{
		return HoDManager.instance.dialogueManager.dialogueItems["ditem_horror"];
	}
	
	//check if the cosmic horror should appear in the map
	protected override bool ShouldAppear()
	{
		if(_tm == null)
		{
			_tm = HoDManager.instance.timeManager;
		}
		
		//its past 2 am in the clock
		bool cond1 = _tm != null && _tm.Hour >= 2;
		
		float avgS = (float)HoDManager.instance.charManager.GetAvgSanity() / 100f;
		float chance = 1f - avgS;
		
		//include a random factor that is dependent on the average sanity of the characters
		bool cond2 = Random.Range(0f,1f) < chance;
		
		//check if the king in yellow npc has already appeared
		bool cond3 = PlayerPrefs.GetInt("kiyAppeared", 0) == 1;
		
		return  cond1 && cond2 && cond3;
	}
	
	//choose a new division for the cosmic horror
	protected override Division ChooseNewLoc (bool useAdjacent)
	{
		if(_currLoc != null)
		{
			if(_currLoc.GetVisibleChars().Count > 0) return _currLoc;
			
			List< Division > adj = _currLoc.adjacent;
			List< Division> divsWithChar = new List<Division>();
			
			//check if there's an adjacent division with a character in it
			foreach(Division d in adj)
			{
				if(d.GetVisibleChars().Count > 0)
				{
					divsWithChar.Add(d);
				}
			}
			if(divsWithChar.Count > 0)
			{	//move to an adjacent division with a character in it
				_hasCaught = true;
				return divsWithChar[Random.Range(0, divsWithChar.Count)];
			}
		}
		//choose an adjacent division to move to
		return base.ChooseNewLoc(useAdjacent);
	}
	
	//fade out from the map for a set number of turns
	protected override void DisappearAndWait(int turns)
	{
		Teleport(false);
		_hasCaught = false;
		_respawnTime = turns;
	}
	
	//cosmic horror executes an action
	protected override void DoAction()
	{
		float rnd = Random.Range(0f,1f);
		int avgS = HoDManager.instance.charManager.GetAvgSanity();
		
		//the cosmic horror is awaiting respawn
		if(_respawnTime > 0)
		{
			--_respawnTime;
			FinishRoam();
		}
		else if(_hasCaught)
		{	//cosmic horror caught a character. Make the cosmic horror disappear for a set number of turns
			DisappearAndWait(cooldownTime);
		}
		else if(avgS > 50 && rnd > 0.5f || avgS <= 50 && rnd > 0.3f || HasCharAdjacent())
		{	//make the cosmic horror move to and adjacent location if one of its preconditions is met
			MoveToAdjacentLoc();
		}
		else if(rnd > 0.2f)
		{
			if(avgS > 50)
			{	//if the average sanity is higher than 50, make the cosmic horror disappear
				Teleport(false);
			}
			else
			{
			{	//if the average sanity is higher than 50, make the cosmic horror appear
				Teleport(true);
			}
		}
		else
		{
			//end turn
			FinishRoam();
		}
	}
}
