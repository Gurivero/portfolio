using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public abstract class NPC : MonoBehaviour {

	
	public string id{get{ return _id; }}
	public CharacterManager.ENPC type{get{ return _type; } set{ _type = value; }}
	public bool isRegularNPC{ get; protected set; }
	
	protected string _id;
	
	protected CharacterManager.ENPC _type;
	protected Division _lastLoc;
	protected Division _currLoc;
	[SerializeField]
	protected Image _token;
	protected bool _firstAppear;
	protected TimeManager _tm;
	protected bool _tempAppear;
	protected string _initDiv;
	protected bool _diagOverride;
	protected bool _showing;
	protected bool _active;
	protected bool _destroy;
	protected bool _hasCaught;
	
	protected List< Zone.EZONETYPE > _roamingZones;
	
	public string diagID
	{	
		get{ return GetDItemID(); }
	}
	
	
	
	public bool diagOverride{ get{ return _diagOverride; }}
	
	public bool firstAppearance{ get{ return _firstAppear; } }
	
	public virtual string GetDItemID(){ return ""; }
	
	public virtual Dialogue GetDialogue(){ return null; }
	
	//get a save string with the npc properties
	public virtual string GetSaveStr()
	{
		string loc = _currLoc != null ? _currLoc.id : "";
		int show = _showing ? 1 : 0;
		int active = _active ? 1 : 0;
		string res = _id + ";" + ((int)_type) + ";" + loc +";" + show + ";" + _initDiv + ";" + active;
		return res;
	}
	
	public bool IsNPCOrigin(Division d)
	{
		return d.id == _initDiv;
	}
	
	//load npc properties from a string
	public virtual void Load(string s)
	{
		string[] split = s.Split(';');
		_id = split[0];
		_type = (CharacterManager.ENPC)int.Parse(split[1]);
		if(split[2] != "")
		{
			SetProperties(_id, _type, HoDManager.instance.map.FindDiv(split[2]));
		}
		int show = int.Parse(split[3]);
		_showing = show == 1;
		_token.enabled = _showing;
		Color c = _token.color;
		c.a = show;
		_token.color = c;
		_initDiv = split[4];
		_active = int.Parse(split[5]) == 1;
		LateInit();		
	}
	
	protected virtual void LateInit(){}
	
	//set the properties of the npc
	public virtual void SetProperties(string id, CharacterManager.ENPC t, Division d)
	{
		_id = id;
		_type = t;
		_initDiv = d != null ? d.id : "";
		if(d != null)
		{
			ChangeLoc(d);
			transform.position = _currLoc.transform.position;
			transform.SetParent(d.transform.parent);
			_currLoc.RearrangeTokens();
		}
		else
		{
			_currLoc = d;
		}
		Init();
	}
	
	
	protected virtual void Init()
	{
	}
	
	void Awake()
	{
		_hasCaught = false;
		_destroy = false;
		isRegularNPC = true;
		_tm = HoDManager.instance.timeManager;
		_firstAppear = true;
		_roamingZones = new List<Zone.EZONETYPE>();	
	}
	
	void Start () {
		_active = true;
	}
	

	protected bool HasCharAdjacent()
	{
		List< Division > adj = _currLoc.adjacent;
		//check the adjacent divisions for characters in them
		return adj.TrueForAll( (Division d) => { return !AdjChars(d); }); 
	}
	
	bool AdjChars(Division d)
	{
		//no visible characters in the division d
		bool cond1 = d.GetVisibleChars().Count > 0;
		
		//no visible npcs in the division d
		bool cond2 = d.npcs.Count > 0;
		return cond1 || cond2;
	}
	
	//check if the npc can move the division d
	bool CanChooseLoc(List< Division > adj, Division d)
	{
		if(_roamingZones == null)
		{
			_roamingZones = new List<Zone.EZONETYPE>();
		}
		
		//there are no roaming zones for this npc or the the zone belonging to d is one of the roaming zones of this npc
		bool cond1 = _roamingZones.Count == 0 || _roamingZones.Contains(d.zone.zoneType);
		
		//there are no npc's in the division
		bool cond2 = d.npcs.Count == 0;
		
		//the npc is not already in the division or there's only an adjacent division to d ... this prevents the npc from getting stuck
		//while reaching a dead end
		bool cond3 = d != _currLoc || d.adjacent.Count == 1;
		
		return cond1 && cond2 && cond3;
	}
	
	//choose a new location for the npc
	protected virtual Division ChooseNewLoc(bool useAdjacent)
	{
		if(_currLoc != null)
		{
			//if the npc is already in a division, choose a division from its adjacents
			List< Division > adj = _currLoc.adjacent;
			if(useAdjacent)
			{
				adj.AddRange(_currLoc.adjacentZone);
			}
			
			int r = Random.Range(0, adj.Count);
			Division d = adj[r];
			int counter = adj.Count;
			
			//find a eligible adjacent division
			while(!CanChooseLoc(adj, d))
			{
				r = Random.Range(0, adj.Count);
				d = adj[r];
				--counter;
				if(counter < 0)
				{
					d = _currLoc;
					break;
				}
			}
			transform.SetParent(d.transform.parent);
			
			return d;
		}
		else //if the npc is not in a division, assign a random division to it
		{
			return HoDManager.instance.mainChar.location.zone.GetRandDiv(true);
		}
	}
	
	//teleport the npc ... it vanishes from the current division and appears at its destination
	public void Teleport(bool andAppear)
	{
		Color c = _token.color;
		c.a = 0f;
		//choose if the npc reappears immediately in the destination
		if(andAppear)
		{
			LeanTween.color(GetComponent< RectTransform >(), c, 0.25f).setOnComplete(Appear);
		}
		else
		{
			//update the dialogues in the division so it removes dialogues regarding the npc
			UpdateDivDialogue();
			LeanTween.color(GetComponent< RectTransform >(), c, 0.25f).setOnComplete( 
			() => { _token.enabled = false; _showing = false; FinishRoam(); });
		}
	}
	
	//update the dialogues in the division so it removes dialogues regarding the npc
	void UpdateDivDialogue()
	{
		//if the npc was in a division and has left it, remove it's dialogues from the previous division
		if(_currLoc != null)
		{
			_currLoc.npcs.Remove(this);
			if( _currLoc.divDialogue != null)
			{
				DivisionDialogue dd = _currLoc.divDialogue;
				string itemID = GetDItemID();
				if(dd.itemConsequences.ContainsKey(itemID))
				{
					dd.itemConsequences.Remove(itemID);
				}
				if(dd.itemIds.Contains(itemID))
				{
					dd.itemIds.Remove(itemID);
				}
			}
		}
	}
	
	//get a possible division for this npc
	Division GetPossibleDiv()
	{
		List< Character > visible = HoDManager.instance.charManager.GetAllVisibleChars();
		Character c = visible[Random.Range(0, visible.Count)];
		if(_roamingZones.Count == 0)
		{
			return c.location.zone.GetRandDiv(true);
		}
		else
		{
			return HoDManager.instance.map.GetRandomDiv(_roamingZones);
		}
	}
	
	//npc visual fade in its current division
	protected void Appear()
	{
		GetComponent< Image >().enabled = true;
		_showing = true;
		_token.enabled = true;
		ChangeLoc(GetPossibleDiv());
		EndMove();
		Color cc = _token.color;
		cc.a = 1f;
		LeanTween.color(GetComponent< RectTransform >(), cc, 0.25f);
		transform.localScale = HoDManager.instance.charManager.GetTokenScale(_id);
		FinishRoam();
	}
	
	//change the division of the npc
	void ChangeLoc(Division d)
	{
		UpdateDivDialogue();
		_lastLoc = _currLoc;
		_currLoc = d;
		_currLoc.npcs.Add(this);
	}
	
	//move the npc to an adjacent division
	public void MoveToAdjacentLoc()
	{
		ChangeLoc(ChooseNewLoc(false));
		LeanTween.move(gameObject, _currLoc.transform.position, 1f).setEase(LeanTweenType.easeInOutQuad).setOnComplete(EndMove);
	}
	
	//check if the npc is showing on the map
	public bool IsVisible()
	{
		return _showing;
	}
	
	protected abstract bool ShouldAppear();
	
	//choose an action for the npc to do this turn
	protected virtual void DoAction()
	{
		float rnd = Random.Range(0f,1f);
		int avgS = HoDManager.instance.charManager.GetAvgSanity();
		if(avgS > 50)
		{
			if(rnd > 0.5f)
			{
				Teleport(false);
			}
			else if(rnd >0.3f)
			{
				MoveToAdjacentLoc();
			}
			else
			{
				FinishRoam();
			}
		}
		else 
		{
			if(rnd > 0.4f)
			{
				MoveToAdjacentLoc();
			}
			else if(rnd >0.1f)
			{
				Teleport(true);
			}
			else
			{
				FinishRoam();
			}
		}
		
	}
	
	//execute npc's turn
	public void NextTurn()
	{
		if(!_active)
		{
			//if npc is not active but is showing on the map, make it fade out
			if(_showing)
			{
				Teleport(false);
			}
			else
			{
				//end turn as the npc is not active
				FinishRoam();
			}
		}
		else if(_showing) //if npc is active and showing, do an action with it
		{
			DoAction();
		}
		else if((_currLoc == null || _currLoc.characters.Count == 0) && ShouldAppear())
		{ //if the npc is active but not showing, make it appear in the map
			Appear();
			_firstAppear = false;
		}
		else
		{
			//end turn
			FinishRoam();
		}
		
	}
	
	//destroy this npc
	protected void DestroyThis()
	{
		_currLoc.divDialogue.FullyRemoveItem(GetDItemID());
		HoDManager.instance.charManager.DestroyNPC(id);
		HoDManager.instance.SaveGame();
		Destroy(this);
	}
	
	//end this npc turn
	protected void FinishRoam()
	{
		if(_currLoc != null)
		{
			_currLoc.RearrangeTokens();
		}
		if(_destroy)
		{ //if marked for removal, destroy this npc
			DestroyThis();
		}
		else
		{
			HoDManager.instance.charManager.FinishedRoaming(id);
		}
	}
	
	//ensure the npc's position in the map is centered to the division he's in
	void EndMove()
	{
		if(_currLoc != null)
		{
			HUD.instance.CenterInZone(_currLoc.zone);
			transform.position = _currLoc.transform.position;
			transform.SetParent(_currLoc.transform.parent);
		}
		FinishRoam();
	}
	
	public void AddRoaming(Zone.EZONETYPE z)
	{
		_roamingZones.Add(z);
	}
	
	public void Deactivate()
	{
		if(_currLoc != null)
		{
			_active = false;
		}
	}
	
	//stun the npc, making it disappear for 3 turns before reappearing in the map
	public void Stun(bool stunNonReg)
	{
		if(stunNonReg || !isRegularNPC)
		{
			DisappearAndWait(3);
		}
	}
	
	//kill the npc, removing it from the game
	public bool Kill(bool killNonRegular)
	{
		Debug.Log("Killin' " + id);
		if(killNonRegular || isRegularNPC)
		{
			Teleport(false);
			_destroy = true;
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void Vanish(int turns)
	{
		DisappearAndWait(turns);
	}
	
	protected abstract void DisappearAndWait(int turns);
}
