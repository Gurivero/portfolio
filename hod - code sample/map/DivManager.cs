using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//class that handles the divisions of the map and respectively spawns items in them
public class DivManager : MonoBehaviour {

	Map _map;
	int _allocatedItems;
	List< string > alloc;
	
	public void SetMap(Map m)
	{
		_map = m;
	}

	//load a string with the properties of the divisions
	public void LoadGame()
	{
		ClearItems();
		string loaddivs = PlayerPrefs.GetString("divs","");
		Division d;
		if(!loaddivs.Equals(""))
		{
			string[] divs= loaddivs.Split('|');
			foreach(string s in divs)
			{
				if(!s.Equals(""))
				{
					string[] div = s.Split(';');
					d = FinDiv(div[0]);
					d.Load(div);
				}
			}
		}
		else
		{
			SpawnItems();
		}
	}
	
	//save the properties of the divisions
	public void SaveGame()
	{
		string save_str = "";
		string aux = "";
		
		foreach(KeyValuePair< string, Zone > z in _map.zones)
		{
			foreach(Division d in z.Value.divs)
			{
				if(d.item != null || d.lockedBy != null)
				{
					aux = d.GetSaveString();
					
					save_str += aux;
				}
			}
		}
		PlayerPrefs.SetString("divs", save_str);
	}
	
	//remove all items from the divisions
	void ClearItems()
	{
		foreach(KeyValuePair< string, Zone > z in _map.zones)
		{
			foreach(Division d in z.Value.divs)
			{
				d.AddItem(null);
				d.lockedBy = null;
			}
		}
	}
	
	//find a division through its id
	public Division FinDiv(string n)
	{
		Division d;
		foreach(KeyValuePair< string, Zone > z in _map.zones)
		{
			d = z.Value.FindDiv(n);
			if(d != null)
			{
				return d;
			}
		}
		return null;
	}
	
	//Lock a division with a key "i"
	void LockDivisions(Item i)
	{
		foreach(string s in i.opensDivs)
		{
			FinDiv(s).lockedBy = i;
		}
		
	}
	
	//spawn items in the divisions
	void SpawnItems()
	{
		alloc = new List<string>();
		ItemManager im = HoDManager.instance.itemManager;
		
		//lock all divisions that are opened with a key
		foreach (KeyValuePair< string, Item > i in im.coreItems) 
		{
			LockDivisions(i.Value);
		}
		
		//handle divisions that are locked without a key
		im.RemoveNoKey();
		
		_allocatedItems = 0;
		do {
			//spawn all other items
			foreach (KeyValuePair< string, Item > i in im.coreItems)
			{
				HandleSpawnItem (i.Value, true);
			}
		} while(_allocatedItems < im.coreItems.Count);
	}
	
	void HandleSpawnItem(Item i, bool prepass)
	{
		if(i.id.Equals("nokey")) return;
		bool containsItem = HoDManager.instance.charManager.inventory.Contains(i);
		
		//if the inventory already contains this item or it is already allocated then skip it
		if(containsItem || alloc.Contains(i.id)) return;
		
		if(prepass)
		{
			if(i.possibleDivs.Count > 0 || i.possibleZones.Count > 0)
			{
				//assign items to predefined divisions
				ChoosePreLocAndSpawn(i);
			}
		}
		else
		{
			if(i.possibleDivs.Count == 0 && i.possibleZones.Count == 0)
			{
				//assign items to random divisions
				ChooseRandLocAndSpawn(i);
			}
		}
	}
	
	//check if there's a path from the division init to the division final without getting stuck
	//this algorithm is rudimentar A*
	public bool HasPathTo(Division init, Division final, List< Division > alreadyChecked, Item it)
	{
		if(final.canSpawn && ( init.Equals(final) || CheckAdjacent (init, final, init.adjacent, alreadyChecked, it)
		   || CheckAdjacent(init, final, init.adjacentZone, alreadyChecked, it)))
		{
			return true;
		}
		else return false;
	}
	
	//check if the adjacent division is transitable
	bool CheckAdjacent(Division init, Division final, List< Division > adj,  List< Division > alreadyChecked, Item it)
	{
		bool aux;
		foreach(Division d in adj)
		{
			//if already went through the division d, skip it
			if(alreadyChecked.Contains(d) || d == null)
			{
				continue;
			}
			else
			{	//add division d to the divisions scanned
				alreadyChecked.Add(d);
			}
			
			// if( no lock or division is locked by key that appears previously) //WARNING: THIS MIGHT NOT BE ENOUGH!
			if(d.lockedBy == null || it != null && d.lockedBy.tier < it.tier ) 
			{
				aux = HasPathTo(d, final, alreadyChecked, it);
				if(aux)
				{
					//there's a transitable path from division init to division final
					return true;
				}
			}
		}
		return false;
	}
	
	//spawn an item in one of its predefined divisions
	void ChoosePreLocAndSpawn(Item it)
	{
		int rand;
		Division aux;
		bool hasPath = false;
		//if  the item has specific possible divisions, choose one of them to spawn the item
		if(it.possibleDivs.Count > 0)
		{
			List< string  > listAux = it.possibleDivs;
			
			//don't spawn items in the characters initial divisions
			if(listAux.Contains(_map.initLocation.id))
			{
				listAux.Remove(_map.initLocation.id);
			}
			do
			{
				rand = Random.Range(0, listAux.Count);
				aux = FinDiv(listAux[rand]);
				List< Division > aChecked = new List<Division>();
				aChecked.Add(_map.initLocation);
				
				//check if there if spawning the item in this division creates a dead end in the map
				if(aux.item == null)
				{
					hasPath = HasPathTo(_map.initLocation, aux, aChecked, it);
				}
				listAux.Remove(listAux[rand]);
			}
			while(!hasPath && listAux.Count > 0);
			if(hasPath)
			{	//spawn the item
				aux.AddItem(it);
				++_allocatedItems;
				alloc.Add(it.id);
			}
		}
		//if the item has specific possible zones, choose a division of one of the zones to spawn the item
		else if(it.possibleZones.Count > 0)
		{
			Zone zaux;
			List< string  > listAux1 = it.possibleZones;
			List< Division  > listAux2;
			do //find a zone and division elegible to spawn the item
			{
				rand = Random.Range(0, listAux1.Count);
				zaux = _map.zones[listAux1[rand]];
				listAux1.Remove(listAux1[rand]);
				listAux2 = new List<Division>(zaux.divs);
				
				//don't spawn items in the characters initial divisions
				if(listAux2.Contains(_map.initLocation))
				{
					listAux2.Remove(_map.initLocation);
				}
				do
				{
					rand = Random.Range(0, listAux2.Count);
					aux = listAux2[rand];
					List< Division > aChecked = new List<Division>();
					aChecked.Add(_map.initLocation);
					//check if there if spawning the item in this division creates a dead end in the map
					if(aux.item == null)
					{
						hasPath = HasPathTo(_map.initLocation, aux, aChecked, it);
					}
					listAux2.Remove(aux);
				}
				while(!hasPath && listAux2.Count > 0);
			}
			while(!hasPath && listAux1.Count > 0);
			if(hasPath)
			{
				aux.AddItem(it);
				++_allocatedItems;
				alloc.Add(it.id);
			}
		}
	}
	
	//choose a random division and spawn the item in it
	void ChooseRandLocAndSpawn(Item it)
	{
		Division d = null;
		do
		{
			d = _map.GetRandomDiv();
		}while(d.item != null);
		
		d.AddItem(it);
		++_allocatedItems;
		alloc.Add(it.id);
	}
}
