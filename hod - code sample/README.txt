This is source code sample from the game Halls of Darkness I worked in.

You'll find 3 folders in this zip:

map - contains a class DivManager.cs that handled item spawning in a map, with dead end checks using A*

AI - contains classes pertinent to AI behaviour of NPC'sample

hodml - contains classes regarding a markup language and parser used in the game for content generation. A hodml example file is also provided.

I hope you like it!