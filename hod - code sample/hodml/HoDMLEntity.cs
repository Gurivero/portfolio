using UnityEngine;
using System.Collections;

public abstract class HoDMLEntity {

	/// <summary>
	/// Sets the properties. the array is comprised of string keys in the par positions and the value in the odd positions.
	/// </summary>
	/// <param name="args">Arguments.</param>
	public void SetProperties(params object[] args)
	{
		string var;
		for(int i = 0 ; i < args.Length ; i += 2)
		{
			var = (string)args[i];
			SetProperty(var, args[i+1]);
		}
	}
	
	public abstract void SetProperty(string arg, object val);
	protected abstract void GenerateID(string prefix = "");
}
