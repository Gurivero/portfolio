using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO; 
using System;

public class HoDMLParser : MonoBehaviour {

	public TextAsset[] files;

	protected struct ParamsToUse
	{
		public string line;
		public HoDMLEntity ent;
		public bool close;
		
		public void SetEntity(HoDMLEntity it)
		{
			ent = it;
		}
		
		public void SetParams(string l, HoDMLEntity i, bool c)
		{
			line = l;
			ent = i;
			close = c;
		}
	}
	
	protected StreamReader _theReader;
	protected ParamsToUse _params;
	protected Stack< string > _tags;
	protected string _filename;
	
	void Awake()
	{
		_tags = new Stack< string >();
	}
	
	//this will parse a file in hodml format
	public void Parse(string filename)
	{ 
		_params = new ParamsToUse ();
		ReadTextAsset(GetTextAsset(filename));
	}
	
	TextAsset GetTextAsset(string s)
	{
		foreach(TextAsset ta in files)
		{
			if(ta.name.Equals(s))
			{
				return ta;
			}
		}
		return null;
	}
	
	void ReadTextAsset(TextAsset ta)
	{
		string[] lines = ta.text.Split('\n');
		string line;
		
		//read each line of the hodml file
		for(int i = 0 ; i< lines.Length; ++i)
		{
			line = lines[i];
			
			//if empty line skip it
			if(line.Equals("")) continue;
			
			
			string[] entries = line.Split(new char[]{'<','>'});
			string tag = entries[1];
			bool close = false;
			
			//if commentary, skip the line
			if(tag.StartsWith("!--")) continue;
			
			//if / symbol found, then assume it's a closing tag
			if(tag.Contains("/"))
			{
				close = true;
				tag = tag.Substring(1);
				
			}
			_params.line = line;
			_params.close = close;
			
			//if the tag starts with f_ then it's a simple element
			if(tag.StartsWith("f_"))
			{
				ReadField(tag.Substring(2));
			}
			//if the tag starts with c_ then it's a complex element
			else if(tag.StartsWith("c_"))
			{
				if(close)
				{
					string lastTag = _tags.Pop();
				}
				else
				{
					_tags.Push(tag);
				}
				tag = tag.Substring(2);
				SendMessage(tag);
			}
		}
	}
	
	//when parsing simple elements, just strip the tag and process the value of the element
	void ReadField(string tag)
	{
		string line = _params.line;
		string[] ids = line.Split (new string[]{"<f_"+tag+">", "</f_"+tag+">"}, StringSplitOptions.RemoveEmptyEntries);
		object[] obj = new object[]{ tag, ids[1]};
		_params.ent.SetProperties(obj);
	}
}