using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class HoDSpecificParser : HoDMLParser {

	//complex element inaccessible
	void inaccessible()
	{
		string line = _params.line;
		string[] ids = line.Split (new string[]{"<c_inaccessible>", "</c_inaccessible>"}, StringSplitOptions.RemoveEmptyEntries);
		string val = ids[1];
		//set the given division as inaccessible to the player
		HoDManager.instance.map.FindDiv(val).SetInaccessible();
	}
		
	//complex element item
	void item()
	{
		Item curr = (Item)_params.ent;
		
		if (_params.close) //closing tag </item>
		{
			ItemManager im = HoDManager.instance.itemManager;
			if(curr.parent != null)
			{
				//the property "tier" is used to define a hierarchy between the items
				curr.SetProperty("tier", curr.parent.tier +1);
				curr.parent.items.Add(curr.id);
				im.AddItem(curr);
			}
			else
			{
				im.AddItem(curr);
			}

			_params.SetEntity(curr.parent);
		}
		else //opening tag <item>
		{
			Item it = new Item (curr, "", new List< string >());
			_params.SetEntity(it);
		}
	}
	
	void character()
	{
		if (_params.close) //closing tag </character>
		{
			Character c = (Character)_params.ent;
			CharacterManager cm = HoDManager.instance.charManager;
			//set the properties of each character
			if(cm.characters.ContainsKey(c.id))
			{
				c.SetProperty("menudesc", cm.characters[c.id].menuDescription);
				c.SetProperty("entity", cm.characters[c.id].charName);
				cm.characters[c.id] = (Character)_params.ent;
			}
			else
			{
				cm.characters.Add(c.id, c);
			}
			
			_params.ent = null;
			
		}
		else //opening tag <character>
		{
			Character c = new Character();
			_params.SetEntity(c);
		}
	}
	
	void dcase()
	{
		ditemdesc();
	}
	
	//complex item ditem
	void ditem()
	{
		if (_params.close) //closing tag </ditem>
		{
			DialogueManager dm = HoDManager.instance.dialogueManager;
			Dialogue de = (Dialogue) _params.ent;
			dm.dialogueItems.Add(de.id, de);
			_params.ent = null;
		}
		else //opening tag <ditem>
		{
			Dialogue c = new Dialogue();
			_params.SetEntity(c);
		}
	}
	
	void ditemdesc()
	{
		if(_params.close)
		{
			((Dialogue)(_params.ent)).PushCurrDialogueEnt();
		}
		else
		{
			((Dialogue)(_params.ent)).SetCurrDialogueEnt(Dialogue.EDItemEntity.Description);
		}
	}
	
	void ditemcons()
	{
		if(_params.close)
		{
			((Dialogue)(_params.ent)).PushCurrDialogueEnt();
		}
		else
		{
			((Dialogue)(_params.ent)).SetCurrDialogueEnt(Dialogue.EDItemEntity.Consequence);
		}
	}
	
	void ditemtext()
	{
		if(_params.close)
		{
			((Dialogue)(_params.ent)).PushCurrDialogueEnt();
		}
		else
		{
			((Dialogue)(_params.ent)).SetCurrDialogueEnt(Dialogue.EDItemEntity.ItemText);
		}
	}
	
	//complex item dialogue
	void dialogue()
	{
		if (_params.close) //closing tag </dialogue>
		{
			Dialogue de = (Dialogue) _params.ent;
			if(de.divType != Division.EDIVTYPE.COUNT)
			{
				HoDManager.instance.dialogueManager.dialogues.Add(de.divType, de);
			}
			else
			{
				HoDManager.instance.dialogueManager.charDialogues.Add(de.id, de);
			}
			_params.ent = null;
		}
		else //opening tag <dialogue>
		{
			Dialogue c = new Dialogue();
			_params.SetEntity(c);
		}
	}
}
