Welcome to my code portfolio. These are samples from some of the work I've made.

HoD - code sample in C#. This contains some of the code I developed for the launched mobile title Halls of Darkness. 
It is a parser to a custom markup language used to generate level content.

Ores. A C++ (libsdl2) game prototype created in 5 days.

Match3. A C++ (libsdl2) game prototype created in a week.

Kinetic Strikers - code sample in C#. A prototype of a sports game created in Unity3D.

I hope you like it and feel free to ask me any questions.

Cheers!