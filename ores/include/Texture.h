#ifndef TEXTURE_H
#define TEXTURE_H

#include <SDL.h>
#include <SDL_ttf.h>
#include <string>

//Texture wrapper class based on LazyFoo's SDL tutorial
class Texture
{
	public:
		//Initializes variables and determines if this texture is used for text rendering
		Texture(SDL_Renderer *r, SDL_Rect *vp);

		//Deallocates memory
		~Texture();

		//Loads image at specified path
		bool LoadFromFile( std::string path );


		//Deallocates texture
		void Free();

		//Renders texture at given point
		void Render( int x, int y );
		void Render( int x, int y, int w, int h );
		void Render(SDL_Rect *quad);


		//Gets image dimensions
		int Width();
		int Height();

		//set texture color
		void SetColor(int r, int g, int b);

		//set texture alpha
		void SetAlpha(int a);

	protected:

		//The actual hardware texture
		SDL_Texture* _texture;

		//Handle to the renderer
		SDL_Renderer *_renderer;

		//Viewport where the texture will be rendered
		SDL_Rect *_viewport;

		//Image dimensions
		int _width;
		int _height;

};
#endif // TEXTURE_H
