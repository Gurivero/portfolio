#ifndef SINGLETON_H
#define SINGLETON_H

//template class for all singletons
template<class T>
class Singleton
{
    public:
        Singleton();
        virtual ~Singleton();

        static T* instance();

    protected:
    private:
        static T* _instance;
};

template <class T>
T* Singleton< T >::_instance = new T();

template <class T>
Singleton< T >::Singleton(){}

template <class T>
Singleton< T >::~Singleton()
{
    if(_instance)
    {
        free(_instance);
    }
}

//method that returns the singleton instance
template <class T>
T* Singleton< T >::instance()
{
    return _instance;
}

#endif // SINGLETON_H
