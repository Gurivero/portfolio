#ifndef GRID_H
#define GRID_H

#include <Global.h>
#include <Cube.h>

class GameState;

class Grid
{
    public:
        Grid( GameState *gs );
        virtual ~Grid();

        //frame update
        void Update();

        //fill columns at the beginning of the game
        void InitialFill();

        //advances the grid one step to the left
        void AdvanceGrid();

        //check if <c> has neighbors with the same color,
        //if there matches, destroy those cubes and check their neighbors
        void CheckCubeDel(Cube *c);

        //initiate the grid with empty cubes
        void InitGrid();

        //returns true if the first column is not empty
        bool InDanger();

    protected:

    private:

    //handle to the game state
    GameState *_gs;

    //texture of the grid floor
    Texture *_floor;

    //floor texture dimensions
    SDL_Rect _renderQuad;

    //grid composition
    //Multidimensional array was the choice for the grid
    //for two reasons:
    //1) Search time of O(1)
    //2) Since the size of the grid is fixed and has
    //a relatively small size, the use of a data structure
    //based in dynamic memory allocation is not crucial

    Cube _grid[GRID_COL_NUM][GRID_ROW_NUM];

    //array with a counter for each column of the grid
    int _gCounter[GRID_COL_NUM];

    //check if two cubes have the same color, if so, delete cube <a> and
    //search the neighbors of <b>
    void CheckSameColor(Cube *a, Cube *b);

    //if there are gaps in a column, collapse the column to fill those gaps
    void CollapseColumn(Cube *c);

    //move columns starting at <start>, dir indicates if the movement is to the
    //left or right, step is the distance to move
    void MoveColumns(int start, int dir, int step = 1);

    //if there's an empty column to the right of <start>, then drag <start> and all
    //the columns to its left to close the gap
    void DragColumn(int start);

    //move the contents of column <from> to column <to>
    void CopyColumn(int from, int to);

    //move the contents of cube <from> to cube <to>
    void CopyCube(Cube *from, Cube *to);

    //fill all cubes of column <idx> with random colors
    void FillColumn(int idx);

    //render the grid floor
    void Render();
};

#endif // GRID_H
