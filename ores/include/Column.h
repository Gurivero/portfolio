#ifndef COLUMN_H
#define COLUMN_H

#include <Global.h>
#include <Cube.h>
#include <SDL.h>

class GameState;

class Column
{
    public:
        Column();
        virtual ~Column();

        //update the cubes in this column
        void Update();

        //fill this column with random cubes
        void Fill();

        //Initialize the column
        void Init(GameState *s, int idx);

        //getter for the GameState
        GameState* GetGameState();

        SDL_Renderer* Renderer();
        SDL_Rect* ViewPort();

        //Add a random cube to the column
        void AddRandomCube();

        //returns the column index
        int ColumnIdx();

        //returns the number of filled cubes
        int NumCubes();

        //returns true when there are no cubes in the column
        bool IsEmpty();

        //returns the cubes array of this column
        Cube* Cubes();

        //Check if cube c has color cl, if so destroy it and
        //check its neighbors
        void CheckAndDestroy(Cube *c, CubeColor cl);


        //Move this column one step in <dir> direction
        void Move(int dir);

        void AddNumCubes(int i);

    protected:
    private:

        //returns the first non empty cube starting from index s
        int FirstFilledIdx(int s);

        //move the column to the right if there are empty columns
        void RearrangeColumn();

        //rearrange the cubes in the column
        void RearrangeCubes();

        //check if cubes have the same color
        //and should be flagged to be destroyed
        void CheckColor(Cube *c, Cube* adj, CubeColor cl);

        int _columnIdx;
        int _numCubes;
        GameState *_gameState;
        Cube _cubes[GRID_ROW_NUM];

        //flag to update column when a cube was destroyed
        bool _changed;
};

#endif // COLUMN_H
