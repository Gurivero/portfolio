#ifndef CUBE_H
#define CUBE_H

#include <Texture.h>
#include <Button.h>

class GameState;

class Cube : public Clickable
{
    public:

        enum CUBE_COLOR { RED = 0, GREEN, BLUE, ORANGE, PURPLE, NONE };

        Cube();
        virtual ~Cube();

        void Update();

        //check the texture, if null then the cube is empty
        bool IsEmpty();

        //returns the color of the cube
        CUBE_COLOR Color();

        //returns the texture of the cube
        Texture* GetTexture();

        //handle the destroy flag for this cube
        bool MarkedToDestroy();
        void MarkToDestroy();

        //destroy cube
        void Destroy();

        void Init(int col, int row, GameState *gs);

        int Col(){ return _col;}

        int Row(){ return _row;}

        //Fill a cube with color <c> and add it to the map of input listeners
        void FillCube(CUBE_COLOR c);

        //Check if mouse is over this object
        bool MouseOver();

        //handle input events.
        void HandleEvent( SDL_Event* e);

    private:

        //callback when clicking this cube
        //static void ClickCube(void *obj);

        //Add a color to the cube. If c is NONE, a random color is attributed
        void AddColor(CUBE_COLOR c);

        //remove the cube's color
        void Clear();

        //filename of the cube texture to use
        std::string GetTexPath();

        //Set the cube coordinates
        void SetCubeCoords();

        Texture *_texture;
        SDL_Renderer *_renderer;
        SDL_Rect *_viewPort;
        CUBE_COLOR _color;

        //handle to the game state
        GameState *_gs;

        //cube position on screen
        int _x;
        int _y;

        //cube size
        int _cubeSize;

        //flag to destroy the cube
        bool _destroy;

        //grid column
        int _col;

        //grid row
        int _row;


};

#endif // CUBE_H
