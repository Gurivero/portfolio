#ifndef INPUTMANAGER_H
#define INPUTMANAGER_H

#include <Manager.h>
#include <list>
#include <map>
#include <utility>
#include <SDL.h>


class Clickable;

typedef std::map< Clickable*, Clickable* > clickMap;

class InputManager : public Manager
{
    public:

        InputManager();
        virtual ~InputManager();

        void Update(float dt);

        void SetState(State * s);

        //Stores a handle to the quit flag defined in main
        void PassQuitFlag( bool *quit);

        //handle listeners for input events
        void AddListener(Clickable *c);
        void RemListener(Clickable *c);

        std::pair< int, int >* MousePointer();

        //remove all listeners
        void Clear();

    protected:
    private:

        void RemFlaggedListeners();

        //quit var to exit the application's main loop
        bool *_quitVar;

        SDL_Event _event;

        std::pair< int, int > _mousePointer;

        clickMap _cMap;
        clickMap _toRem;
};

#endif // INPUTMANAGER_H
