#ifndef MENUUI_H
#define MENUUI_H

#include <UI.h>
#include <FontTexture.h>

class MenuState;

class MenuUI : public UI
{
    public:
        MenuUI(MenuState *ms);
        virtual ~MenuUI();

        void Update(float dt);
        void Render(float dt);

    protected:
    private:
        //handle to the menu state
        MenuState *_menuState;

        //menu text
        FontTexture *_clickStart;

        //title text
        FontTexture *_title;
};

#endif // MENUUI_H
