#ifndef MAIN_H
#define MAIN_H


class Main
{
    public:
        Main();
        virtual ~Main();
    protected:
    private:
        int main(int argc, char* argv[]);
};

#endif // MAIN_H
