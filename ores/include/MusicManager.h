#ifndef MUSICMANAGER_H
#define MUSICMANAGER_H

#include <Manager.h>
#include <SDL_mixer.h>


class MusicManager : public Manager
{
    public:
        MusicManager();
        virtual ~MusicManager();

        void Update(float dt){}

        void PlayMusic(std::string m);
        void StopMusic();

        void PlaySound(std::string s);

    protected:
    private:

        //game music
        Mix_Music *_music;

        //game sound
        Mix_Chunk *_sound;

};

#endif // MUSICMANAGER_H
