#ifndef GLOBAL_H
#define GLOBAL_H

//In here store all the global constants

//Screen dimension constants
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

//dimensions of the game grid
const int GRID_ROW_NUM = 10;
const int GRID_COL_NUM = 15;

//number of cube colors
const int NUM_COLORS = 5;

//number of initially filled columns
const int INIT_FILLED_COLS = 5;

//timer for grid push
const int TIME_ADVANCE_GRID = 5.f;

//points for each cube destroyed
const int CUBE_POINTS = 1;

//screen flash speed
const float FLASH_SCREEN_SPEED = 100.f;

#endif // GLOBAL_H
