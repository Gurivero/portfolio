#ifndef GAMESTATE_H
#define GAMESTATE_H

#include <State.h>
#include <Grid.h>
#include <GameUI.h>

class GameState : public State
{
    public:

        enum GAME_PHASE{ PREGAME, GAME, GAMEOVER};

        GameState(SDL_Renderer *r);
        virtual ~GameState();

        //Initializes the game state
        void Start();

        //Stops the game state
        void Stop();

        //Updates the game state
        void Update(float dt);

        //The viewport rect
        SDL_Rect* GameViewPort();

        //make the grid move towards the end zone
        void AdvanceGrid();

        //check if <c> has neighbors with the same color,
        //if there matches, destroy those cubes and check their neighbors
        void CheckCubeDel(Cube *c);

        //returns a handle for the UI
        GameUI* GetUI();

        //returns the grid timer
        float GridTimer();

        //returns the current score
        int Score();

        //increments the score
        void IncrScore(int i);

        //when game over condition was reached
        void LostGame();

        //returns the game phase
        GAME_PHASE Phase();

    protected:

        //updates the contents of the game state and renders them
        void UpdateContents(float dt);

    private:

        //flash screen when on last column
        void FlashScreen(float dt);

        //go to game state
        static void GoToGame(void *o);

        //update and render the UI viewport
        void UpdateUI();

        //update and render the game viewport
        void UpdateGame();

        //Viewport for the game UI
        SDL_Rect _topVPort;

        //Viewport for the game itself
        SDL_Rect _gameVPort;

        //timer for the columns to advance;
        float _timer;

        //game score
        int _score;

        //the multidimensional array with the cubes
        Grid _grid;

        //UI for this state
        GameUI _ui;

        //game phase
        GAME_PHASE _phase;

        //current flash screen value for red
        float _flashValue;

        //current flash progression (1 to red, -1 to black)
        int _flashDir;

        //current flash red value target
        float _flashTarget;

};

#endif // GAMESTATE_H
