#ifndef CLICKABLE_H
#define CLICKABLE_H

#include <SDL.h>
#include <InputManager.h>

//Singleton that handles all input events
typedef Singleton< InputManager > inputManager;

class Clickable
{
    public:

        //handle input events.
        virtual void HandleEvent( SDL_Event* e) = 0;

        //Check if mouse is over this object
        virtual bool MouseOver() = 0;

};

#endif // CLICKABLE_H
