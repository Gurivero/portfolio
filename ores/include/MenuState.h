#ifndef MENUSTATE_H
#define MENUSTATE_H

#include <State.h>
#include <MenuUI.h>
#include <Button.h>

class MenuState : public State
{
    public:
        MenuState(SDL_Renderer *r);
        virtual ~MenuState();
        void Start();
        void Stop();
        void Update(float dt);

        //The viewport rect
        SDL_Rect* ViewPort();

    protected:
        void UpdateContents(float dt);
    private:

        static void GoToGame(void *o);

        //Viewport for the game itself
        SDL_Rect _viewPort;

        float _timer;

        //UI for this state
        MenuUI _ui;

        //button filling all the menu to start the game
        Button _startButton;
};

#endif // MENUSTATE_H
