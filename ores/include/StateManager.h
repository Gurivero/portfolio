#ifndef STATEMANAGER_H
#define STATEMANAGER_H

#include <Manager.h>
#include <SDL.h>


class StateManager : public Manager
{

    public:

        enum GAME_STATE { GAME, MENU, NONE };

        StateManager();
        virtual ~StateManager();

        void Start(SDL_Renderer* r);
        void Stop();
        void Update(float dt);
        void ChangeState(GAME_STATE gs);


    protected:
    private:

        void SetState(GAME_STATE s);

        SDL_Renderer *_renderer;
        GAME_STATE _changeState;
};

#endif // STATEMANAGER_H
