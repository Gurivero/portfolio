#ifndef STATE_H
#define STATE_H

#include <SDL.h>
#include <iostream>
#include <Global.h>

class State
{
    public:
        State(SDL_Renderer *r);
        virtual ~State();

        virtual void Start() = 0;
        virtual void Stop() = 0;
        virtual void Update(float dt) = 0;
        SDL_Renderer* Renderer();


    protected:
        SDL_Renderer *_renderer;
        virtual void UpdateContents(float dt) = 0;


    private:
};

#endif // STATE_H
