#ifndef TEX_H
#define TEX_H

class Texture
{
    public:

        Tex();
        ~Tex();

        //Loads image at specified path
        bool LoadFromFile( std::string path );

        //Deallocates texture
        void Free();

        //Renders texture at given point
        void Render( int x, int y );

        //getters
        int Width();
        int Height();

    private:

        //Image dimensions
        int _width;
        int _height;

        //The actual hardware texture
        SDL_Texture* _texture;
};

#endif // TEX_H


