#ifndef FONTTEXTURE_H
#define FONTTEXTURE_H

#include <Texture.h>


class FontTexture : public Texture
{
    public:
        FontTexture(SDL_Renderer *r, SDL_Rect *vp);
        virtual ~FontTexture();

        //prepare text to be rendered
		void SetTextToRender(std::string text, SDL_Color color);
		void SetTextToRender(std::string text);

        //set the font size
        void SetFontSize(int i);

        //set the font color
        void SetFontColor(int r, int g, int b);
        void SetFontColor(SDL_Color c);

        //flash the font with defined interval <interval>
        void FlashFont(float dt, float interval);

    protected:

	    //load the text into the texture
		bool LoadFromRenderedText(std::string text, SDL_Color textColor);

        //load the font used for text rendering
	    void LoadFont();

    private:

        //Globally used font
        TTF_Font *_font;

        //size of the open font
        int _fontSize;

        //stored text
        std::string _text;

        //current color of the font
        SDL_Color _fontColor;

        //timer used in flash font
        float _flashTimer;

        //flag used in the flash algorithm
        bool _useColorFlash;
};

#endif // FONTTEXTURE_H
