#ifndef BUTTON_H
#define BUTTON_H

#include <Clickable.h>

class Button : public Clickable
{
    public:
        typedef void (*voidmethod)(void *);

        Button(int x, int y , int w, int h);
        Button();

        virtual ~Button();

        //handle input events.
        void HandleEvent( SDL_Event* e);

        //set the click callback for this button
        void SetClickCallback(voidmethod, void* o);

        //set position and size of this button
        void SetButtonProps(int x, int y, int w, int h);

        //Check if mouse is over this object
        bool MouseOver();

        //execute the callback
        void CallBack();

    private:
        //callback to click on the button
        void (*click)(void *obj);

        //button dimensions
        int _x, _y, _width, _height;

        //pointer to object that owns the button
        void* _obj;
};

#endif // BUTTON_H

