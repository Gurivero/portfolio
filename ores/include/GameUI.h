#ifndef GAMEUI_H
#define GAMEUI_H

#include <UI.h>
#include <FontTexture.h>

class GameState;

class GameUI : public UI
{
    public:
        GameUI(GameState* gs);
        virtual ~GameUI();

        void Update(float dt);
        void Render(float dt);

    protected:
    private:

        //timer until grid advances
        FontTexture *_gridTimer;

        //game score
        FontTexture *_score;

        //initial text : "Ready?" "Go!"
        FontTexture *_initText;

        //game over score text
        FontTexture *_goScoreText;

        //handle to the game state
        GameState* _gs;


        std::string Stringify(float f);
        std::string Stringify(int i);

};

#endif // GAMEUI_H
