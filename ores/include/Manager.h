#ifndef MANAGER_H
#define MANAGER_H

#include <Singleton.h>
#include <iostream>

class State;

class Manager
{
    public:
        Manager();
        virtual ~Manager();
        virtual void Update(float dt) = 0;
        virtual void SetState(State *s);
    protected:
        State *_state;
    private:
};

#endif // MANAGER_H
