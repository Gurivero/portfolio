#ifndef UI_H
#define UI_H

#include <Texture.h>

class UI
{
    public:
        UI();
        virtual ~UI();

        virtual void Update(float dt) = 0;

        virtual void Render(float dt) = 0;

        //returns the mouse over Texture
        Texture* MouseOverTex();

    protected:

        Texture* _mouseOver;

    private:
};

#endif // UI_H
