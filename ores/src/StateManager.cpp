#include "StateManager.h"

#include <GameState.h>
#include <MenuState.h>
#include <InputManager.h>

typedef Singleton< InputManager > inputManager;

StateManager::StateManager():
    _changeState(NONE)
{
}

StateManager::~StateManager()
{
    //dtor
}

void StateManager::Start(SDL_Renderer *r)
{
    _renderer = r;

    //make the menu state as the current state
    _state = new MenuState(_renderer);
    _state->Start();
}

void StateManager::Update(float dt)
{
    if(_changeState != NONE)
    {
        //request for state change
        SetState(_changeState);
    }
    else
    {
        //update the current state and render its contents
        _state->Update(dt);
    }
}

void StateManager::ChangeState(GAME_STATE s)
{
    //flag for request for state change
    _changeState = s;
}

void StateManager::SetState(GAME_STATE s)
{
    inputManager::instance()->Clear();
    if(_state)
    {
        _state->Stop();
        delete _state;
    }

    switch(s)
    {
    case GAME:
        _state = new GameState(_renderer);
        break;
    case MENU:
        _state = new MenuState(_renderer);
        break;
    default:
        break;
    }

    //clear the state change flag
    _changeState = NONE;

    //start the new state
    _state->Start();

    //update the input manager with the new state
    inputManager::instance()->SetState(_state);
}
