#include "Singleton.h"

T* Singleton::_instance = new T();

Singleton::Singleton()
{
    //ctor
}

Singleton::~Singleton()
{
    //dtor
}

T* Singleton::instance()
{
    return _instance;
}
