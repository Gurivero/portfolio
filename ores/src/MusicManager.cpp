#include "MusicManager.h"

MusicManager::MusicManager():
    _music(NULL),
    _sound(NULL)
{
    //ctor
}

MusicManager::~MusicManager()
{
    if(_music)
    {
        StopMusic();
        Mix_FreeMusic(_music);
    }
    if(_sound)
    {
        Mix_FreeChunk(_sound);
    }
}

void MusicManager::PlayMusic(std::string m)
{
    if(_music)
    {
        //stop music
        StopMusic();
        Mix_FreeMusic(_music);
    }

    _music = Mix_LoadMUS( m.c_str() );
	if( _music != NULL )
	{
	    Mix_VolumeMusic(50);
        Mix_PlayMusic( _music, -1 );
	}
	else
    {
        std::cout << "Failed to load music " << m << "\n";
    }
}

void MusicManager::StopMusic()
{
    Mix_HaltMusic();
}

void MusicManager::PlaySound(std::string s)
{
    if(_sound)
    {
        Mix_FreeChunk(_sound);
    }

    _sound = Mix_LoadWAV( s.c_str() );
    if(_sound != NULL)
    {
        Mix_VolumeChunk(_sound, 10);
        Mix_PlayChannel(-1,_sound, 0);
    }
    else
    {
        std::cout << "Failed to load sound " << s << "\n";
    }
}
