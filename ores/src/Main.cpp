#include <iostream>
#include <ctime>
#include <time.h>

#include <SDL.h>
#include <SDL_ttf.h>
#include <SDL_image.h>
#include <SDL_mixer.h>

#include <Global.h>
#include <State.h>
#include <InputManager.h>
#include <StateManager.h>

//Singleton that handles all input events
typedef Singleton< InputManager > inputManager;

//Singleton that handles the game states
typedef Singleton< StateManager > stateManager;



//The window to render
SDL_Window *_window = NULL;

//The renderer
SDL_Renderer *_renderer = NULL;

//Time at the last frame
double _lastFrameTime = clock();

//Time between the last frame and the current frame
float _deltaTime = 0.0;

bool InitSDL()
{
    //flag to determine if sdl has initiated correctly
	bool hasInit = true;

	//Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO | SDL_INIT_AUDIO ) < 0 )
	{
		std::cout << "SDL could not initialize! SDL Error: " << SDL_GetError() << "\n";
		hasInit = false;
	}

	//Set texture filtering to linear
    if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
    {
        std::cout << "Warning: Linear texture filtering not enabled!";
    }

    //Create window
    _window = SDL_CreateWindow( "Clone of Ores", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
    if(!_window)
    {
        std::cout << "Window could not be created! SDL Error: %s\n", SDL_GetError();
        hasInit = false;
    }
    else
    {
        //Create renderer for window
        _renderer = SDL_CreateRenderer( _window, -1, SDL_RENDERER_ACCELERATED );
        if(!_renderer)
        {
            std::cout << "Renderer could not be created! SDL Error: %s\n", SDL_GetError();
            hasInit = false;
        }
        else
        {
            //Initialize renderer color
            SDL_SetRenderDrawColor( _renderer, 0xFF, 0xFF, 0xFF, 0xFF );

            //Initialize PNG loading
            int imgFlags = IMG_INIT_PNG;
            if( !( IMG_Init( imgFlags ) & imgFlags ) )
            {
                std::cout << "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError();
                hasInit = false;
            }

            //Initialize SDL_ttf
            if( TTF_Init() == -1 )
            {
                std::cout << "SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError();
                hasInit = false;
            }

            //Initialize SDL_mixer
            if( Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 2048 ) < 0 )
            {
                std::cout << "SDL_mixer could not initialize! SDL_mixer Error: " << Mix_GetError() << "\n";
                hasInit = false;
            }
        }
    }

	return hasInit;
}

void CloseSDL()
{
    //Destroy window
	SDL_DestroyWindow( _window );
	_window = NULL;

    //quit SDL_mixer
    Mix_CloseAudio();
	Mix_Quit();

	//quit SDL_image
	IMG_Quit();

    //SDL own quit method
	SDL_Quit();
}

//Calculate the delta time between the current and the previous frames
void UpdateTime()
{
    double now = clock();
    _deltaTime = (now - _lastFrameTime) / CLOCKS_PER_SEC;
    _lastFrameTime = now;
}

void Update()
{
    //calculate the delta time
    UpdateTime();

    //update the input events
    inputManager::instance()->Update(_deltaTime);

    //update the current state
    stateManager::instance()->Update(_deltaTime);
}

int main(int argc, char* argv[])
{
    //Check for successful SDL initialization
    if(!InitSDL())
    {
        std::cout << "error initializing SDL. Quitting now.";
        return 0;
    }

    //initialize the prng. This will be responsible for the randomness
    //of the cubes
    srand(time(NULL));

    //var to determine when the main loop ends
    bool quit = false;

    //pass a handle to the quit flag to the input manager,
    //there the flag is updated
    inputManager::instance()->PassQuitFlag(&quit);

    //init the game menu
    stateManager::instance()->Start(_renderer);

    //main loop
    while(!quit)
    {
        //execute the update chain
        Update();
    }


    CloseSDL();

    return 0;
}
