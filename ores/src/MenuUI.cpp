#include "MenuUI.h"

#include <MenuState.h>

MenuUI::MenuUI(MenuState *ms)
{
    _menuState = ms;

    SDL_Renderer *r = ms->Renderer();
    SDL_Rect *vp = ms->ViewPort();

    SDL_Color textColor = {255, 255, 255};

    _title = new FontTexture(r, vp);
    _title->SetFontSize(30);
    _title->SetFontColor(textColor);

    _clickStart = new FontTexture(r, vp);
    _clickStart->SetFontSize(15);
    _clickStart->SetFontColor(textColor);


    std::string aux = "CLONE OF ORES";
    _title->SetTextToRender(aux);

    aux = "click to START!";
    _clickStart->SetTextToRender(aux);
}

MenuUI::~MenuUI()
{
    //dtor
}

void MenuUI::Update(float dt)
{
    //render this UI contents
    Render(dt);
}

void MenuUI::Render(float dt)
{
    int sw = SCREEN_WIDTH;
    int sh = SCREEN_HEIGHT;
    int x,y;

    x = (sw/2) - _title->Width() /2;
    y = (sh/3) - _title->Height() /2;

    _title->Render( x, y);

    x = (sw/2) - _clickStart->Width() /2;
    y += _title->Height() * 2;

    _clickStart->FlashFont(dt, 1.f);
    _clickStart->Render(x, y);
}
