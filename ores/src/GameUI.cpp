#include "GameUI.h"
#include <stdio.h>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <GameState.h>


GameUI::GameUI(GameState* gs):
    _gs(gs)
{
    SDL_Renderer *r = _gs->Renderer();
    SDL_Rect *vp = _gs->GameViewPort();

    //initialize the mouse pointer overlay for the game grid
    _mouseOver = new Texture(r, vp);
    _mouseOver->LoadFromFile("./assets/textures/mouse_over.png");

    SDL_Color textColor = {255, 255, 255};

    //timer for the grid advance
    _gridTimer = new FontTexture(r, vp);
    _gridTimer->SetFontSize(15);
    _gridTimer->SetFontColor(textColor);

    //game score text
    _score = new FontTexture(r, vp);
    _score->SetFontSize(15);
    _score->SetFontColor(textColor);

    //initial text
    _initText = new FontTexture(r, vp);
    _initText->SetFontSize(50);
    _initText->SetFontColor(textColor);

    //game over score text
    _goScoreText = new FontTexture(r, vp);
    _goScoreText->SetFontSize(30);
    _goScoreText->SetFontColor(textColor);
}

GameUI::~GameUI()
{
    //dtor
    delete _mouseOver;
    delete _gridTimer;
    delete _score;
}

void GameUI::Update(float dt)
{

    float t = TIME_ADVANCE_GRID - _gs->GridTimer();

    float time = _gs->GridTimer();
    std::string aux;

    switch(_gs->Phase())
    {
    case GameState::PREGAME:

        _goScoreText->SetTextToRender(" ");
        if(time < 2.f)
        {
            _initText->SetTextToRender("Ready?");
        }
        else
        {
            _initText->SetTextToRender("GO!");
        }

        break;

    case GameState::GAME:

        //update grid timer text
        aux = "Push: " + Stringify(t);
        _gridTimer->SetTextToRender(aux);

        //update the game score
        aux = "Score: " + Stringify(_gs->Score());
        _score->SetTextToRender(aux);

        break;

    case GameState::GAMEOVER:

        _initText->SetTextToRender("Game Over");
        aux = "Score: " + Stringify(_gs->Score());
        _goScoreText->SetTextToRender(aux);
        break;
    default:
        break;
    }

    //render this UI
    Render(dt);
}


void GameUI::Render(float dt)
{
    int sw = SCREEN_WIDTH;
    int sh = SCREEN_HEIGHT;

    if(_gs->Phase() == GameState::GAME)
    {
        _gridTimer->Render( sw - _gridTimer->Width() * 1.1f , sh * 0.1f);
        _score->Render( sw * 0.03f , sh * 0.1f);
    }
    else
    {
        int x, y;
        x = (sw/2) - _initText->Width() / 2;
        y = (sh/3) - _initText->Height() / 2;

        _initText->Render(x,y);

        x = (sw/2) - _goScoreText->Width() / 2;
        y +=  _initText->Height() * 2;
        _goScoreText->Render(x, y);
    }

}

std::string GameUI::Stringify(float f)
{
    std::ostringstream ss;
    ss << std::fixed << std::setprecision(1) << f;
    return ss.str();
}

std::string GameUI::Stringify(int i)
{
    std::ostringstream ss;
    ss << i;
    return ss.str();
}
