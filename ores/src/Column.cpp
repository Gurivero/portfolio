#include "Column.h"
#include <stdio.h>
#include <GameState.h>

Column::Column() :
    _columnIdx(-1),
    _numCubes(0),
    _gameState(NULL),
    _changed(false)
{
}

Column::~Column()
{
    //dtor
}

void Column::Update()
{

 //   RearrangeColumn();

    if( _changed)
    {
        _changed = false;
        printf("------- %d -------\n", _columnIdx);
        //Rearrange the column
        RearrangeCubes();
    }
    //only update cubes that are actually visible
    for(int i = 0 ; i < _numCubes ; ++i)
    {
        _cubes[i].Update();
    }
}

//fill the column with random cubes
void Column::Fill()
{
    for(int i = 0 ; i < GRID_ROW_NUM; ++i)
    {
        AddRandomCube();
    }
}

void Column::AddRandomCube()
{
    _cubes[_numCubes].Activate();
}

void Column::Init(GameState *s, int idx)
{
    //column index
    _columnIdx = idx;

    //handle for the Game State
    _gameState = s;

    //pass the handle of the column to each cube in it
    for(int i = 0 ; i < GRID_ROW_NUM ; ++i)
    {
        _cubes[i].SetColumn(this);
    }
}

GameState* Column::GetGameState()
{
    return _gameState;
}

int Column::NumCubes()
{
    return _numCubes;
}

int Column::ColumnIdx()
{
    return _columnIdx;
}

void Column::CheckAndDestroy(Cube* c, CubeColor cl)
{
    int cubeID = c->ID();
    int upper = cubeID + 1;
    int bottom = cubeID -1;
    int left = _columnIdx - 1;
    int right = _columnIdx + 1;

    Cube *adj;

    if(upper < _numCubes)
    {
        adj = &_cubes[upper];
        CheckColor(c, adj, cl);
    }
    if(bottom > -1 )
    {
        adj = &_cubes[bottom];
        CheckColor(c, adj, cl);
    }
    if(left > -1)
    {
        adj = &(_gameState->Grid()[left].Cubes()[cubeID]);
        CheckColor(c, adj, cl);
    }
    if(right < GRID_COL_NUM)
    {
        adj = &(_gameState->Grid()[right].Cubes()[cubeID]);
        CheckColor(c, adj, cl);
    }

    //if the cube c was flagged to destruction, destroy it
    if(c->MarkedToDestroy())
    {
        //destroy the cube...
        c->Destroy();

        printf("COLUMN:%d numcubes left:%d\n", _columnIdx, _numCubes);

        //and mark this column for update
        _changed = true;
    }
}

void Column::CheckColor(Cube *c, Cube *adj, CubeColor cl)
{
    //if <adj> is of the same color of <c> then mark <c> for destruction
    if(adj->Color() == cl)
    {
        c->MarkToDestroy();

        //if <adj> is not marked for destruction check it's neighbors
        //for the same color
        if(!adj->MarkedToDestroy())
        {
            adj->GetColumn()->CheckAndDestroy(adj, cl);
        }
    }
}

Cube* Column::Cubes()
{
    return _cubes;
}

void Column::RearrangeCubes()
{
    int idx = 0;
    int idx2 = 0;

    //traverse the column
    while(idx < GRID_ROW_NUM)
    {
        //when finding an empty cube...
        if(_cubes[idx].IsEmpty())
        {
            //...find the first non-empty cube above it, and replace it
            idx2 = FirstFilledIdx(idx);
            if(idx2 < GRID_ROW_NUM)
            {
                _cubes[idx].CopyCube(&_cubes[idx2], true);
            }
        }

        ++idx;
    }
}

int Column::FirstFilledIdx(int s)
{
    //traverse the column upwards
    while(s < GRID_ROW_NUM)
    {
        //if non empty, return the index
        if(!_cubes[s].IsEmpty())
        {
            return s;
        }

        ++s;
    }
    //didn't find any non empty cubes, return GRID_ROW_NUM
    return GRID_ROW_NUM;
}

void Column::Move(int dir)
{
    if(_columnIdx == 0 && dir == -1)
    {
        //first column has cubes, so it is game over
        if(_numCubes > 0)
        {
            //LOST THE GAME
            printf("lost the game");
            _gameState->Stop();
        }
    }
    else
    {
        Column *to = &(_gameState->Grid()[_columnIdx + dir]);
        int totalCubes = _numCubes;
        int totalCubesTo = to->NumCubes();

        //if the column where we're going to copy has more cubes than this column,
        //then erase the extra cubes
        for(int j = totalCubes ; j < totalCubesTo ; ++j)
        {
            to->Cubes()[j].Destroy();
        }

        //copy every cube from this column to column <to>
        for(int i = 0 ; i < totalCubes; ++i)
        {
            to->Cubes()[i].CopyCube(&_cubes[i], true);
        }
    }
}

void Column::AddNumCubes(int i)
{
    _numCubes += i;
}

bool Column::IsEmpty()
{
    return _numCubes == 0;
}
