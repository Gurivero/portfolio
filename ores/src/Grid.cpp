#include "Grid.h"
#include <stdio.h>
#include <GameState.h>
#include <MusicManager.h>

typedef Singleton< MusicManager > musicManager;

Grid::Grid(GameState *gs):
    _gs(gs)
{

    _floor = new Texture(_gs->Renderer(), _gs->GameViewPort());
    _floor->LoadFromFile("./assets/textures/grey.png");
}

Grid::~Grid()
{
    //dtor
}

void Grid::Update()
{
    Cube *c = NULL;

    int auxCounter;

    //update all the game columns from right to left
    for(int i = GRID_COL_NUM-1; i >= 0; --i)
    {
        //this counter serves to optimize performance, reducing the number of elements traversed
        auxCounter = 0;

        //only update the column if it has non-empty cubes
        if(_gCounter[i] > 0)
        {
            //update all cubes in the column, from bottom to top
            for(int j = 0 ; j < GRID_ROW_NUM; ++j)
            {
                c = &_grid[i][j];

                //if the cube is empty. drop the upper cubes on top of it
                if(c->IsEmpty() && _gCounter[i] > 0)
                {
                    CollapseColumn(c);
                }

                //internal cube update
                c->Update();

                ++auxCounter;

                //all visible cubes of the column were updated, no need to keep checking this column
                if(auxCounter == _gCounter[i])
                {
                    break;
                }
            }
        }
        else
        {
            //it's an empty column, drag the columns to the left to close the gap
            if(_gCounter[i] == 0)
            {
                DragColumn(i);
            }
        }

    }

    //render the floor
    Render();
}

void Grid::Render()
{
    _floor->Render(&_renderQuad);
}

void Grid::InitGrid()
{
    for(int i = 0 ; i < GRID_COL_NUM; ++i)
    {
        //each column starts with 0 cubes;
        _gCounter[i] = 0;

        //initiate each cube of every column
        for(int j = 0; j < GRID_ROW_NUM; ++j)
        {
            _grid[i][j].Init(i,j, _gs);
        }
    }

    int vpHeight = _gs->GameViewPort()->h;

    int csize = vpHeight * 0.7f / GRID_ROW_NUM;

    _renderQuad.w = GRID_COL_NUM * csize;
    _renderQuad.h = vpHeight * 0.1f;

    _renderQuad.x = SCREEN_WIDTH - _renderQuad.w;
    _renderQuad.y = vpHeight * 0.9f;
}

void Grid::DragColumn(int start)
 {
    int nonEmpty = start;

    //find the first non-empty column to the left
    while(nonEmpty > 0 && _gCounter[nonEmpty] == 0)
    {
        --nonEmpty;
    }

    //there's a non-empty column, drag that column and all to the left of it to close the gap
    if(_gCounter[nonEmpty] > 0)
    {
        //this is the size of the gap
        int step = start - nonEmpty;
        MoveColumns(nonEmpty, 1, step);
    }
 }

 void Grid::MoveColumns(int start, int dir, int step)
 {
    //set the limit according to the opposite of the direction
    //we're traversing the grid
    int limit = GRID_COL_NUM;

    if(dir == 1)
    {
        limit = -1;
    }

    //the column we want to move to
    int colToSwap = start + step * dir;

    //move the columns starting from <start>
    while(start != limit)
    {
        //Move the contents of <start> column into <colToSwap> column
        CopyColumn(start, colToSwap);

        //the next column to move
        start -= dir;

        //the next column to be overwritten
        colToSwap = start + step * dir;
    }
 }

 void Grid::CopyColumn(int from, int to)
 {
    Cube *f, *t;
    for(int i = 0 ; i < GRID_ROW_NUM; i++)
    {
        f = &_grid[from][i];
        t = &_grid[to][i];

        //find the correspondent cube to each row and copy it
        CopyCube(f, t);
    }

    //update the counters of the two columns
    _gCounter[to] = _gCounter[from];
    _gCounter[from] = 0;
 }

 void Grid::CollapseColumn(Cube *c)
 {
    int cl = c->Col();
    int r = c->Row();

    Cube *up = c;

    //find the first top non empty cube
    while(r < GRID_ROW_NUM - 1 && up->IsEmpty())
    {
        ++r;
        up = &_grid[cl][r];
    }

    //if a top non-empty cube was found, move accordingly
    //all the cubes from the initial cube to the top cube found
    if(!up->IsEmpty() )
    {
        int diff = r - c->Row();

        Cube *bot, *top;

        for(int i = c->Row(); i < GRID_ROW_NUM - diff; ++i)
        {
            //bot is where the top cube will drop to
            bot = &_grid[cl][i];
            top = &_grid[cl][i + diff];

            //copy the top cube to the bottom cube
            CopyCube(top, bot);
        }
    }
 }

 void Grid::CopyCube(Cube *from, Cube *to)
 {
    //only copy if the <from> cube is not empty
    if(!from->IsEmpty())
    {
        //copy the <from> cube color to the <to> cube
        to->FillCube(from->Color());

        //clear the <from> cube
        from->Destroy();
    }
 }

 void Grid::FillColumn(int idx)
 {
   for(int i = 0 ; i < GRID_ROW_NUM; ++i)
    {
        //assign a random color to each cube
        _grid[idx][i].FillCube(Cube::NONE);
    }

    //set the counter to full column
    _gCounter[idx] = GRID_ROW_NUM;
 }

 void Grid::InitialFill()
 {
    for(int i = 1 ; i <= INIT_FILLED_COLS ; ++i)
    {
        //fill the column with visible cubes
        FillColumn(GRID_COL_NUM - i);
    }
 }


 void Grid::AdvanceGrid()
 {
    //the furthermost column to the left has elements, so it's game over
    if(_gCounter[0] > 0)
    {
        _gs->LostGame();
    }
    else
    {
        //move all columns to the left
        MoveColumns(1, -1, 1);

        //fill the right most column with new cubes
        FillColumn(GRID_COL_NUM - 1);

        //play sound
        musicManager::instance()->PlaySound("./assets/sound/sfx/column.wav");
    }
 }

 void Grid::CheckCubeDel(Cube *o)
 {
    Cube *n;
    int c = o->Col();
    int r = o->Row();

    //we'll check all four neighbors of <o> for same color matches
    //if there's a match, then propagate to the next neighbors

    if(c > 0) //cube to the left
    {
        n = &_grid[c - 1][r];
        CheckSameColor(o, n);
    }
    if(c < GRID_COL_NUM -1) // cube to the right
    {
        n = &_grid[c + 1][ r];
        CheckSameColor(o, n);
    }
    if(r > 0) // cube to the bottom
    {
        n = &_grid[c][ r - 1 ];
        CheckSameColor(o, n);
    }
    if(r < GRID_ROW_NUM - 1) //cube to the top
    {
        n = &_grid[c][ r +1 ];
        CheckSameColor(o, n);
    }

    //there are neighbors with the same color, destroy the cube
    if(o->MarkedToDestroy())
    {
        --_gCounter[o->Col()];

        _gs->IncrScore(CUBE_POINTS);

        musicManager::instance()->PlaySound("./assets/sound/sfx/destroy.wav");

        o->Destroy();
    }
 }

 void Grid::CheckSameColor(Cube *a, Cube* b)
 {
    //if the cubes have the same color, mark the first to deletion, and check the neighbors of the second
    if(a->Color() == b->Color())
    {
        a->MarkToDestroy();
        if(!b->MarkedToDestroy())
        {
            //now check b for neighbors with the same color
            CheckCubeDel(b);
        }
    }
 }

 bool Grid::InDanger()
 {
     return _gCounter[0] > 0;
 }
