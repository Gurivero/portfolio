#include "FontTexture.h"
#include <iostream>


FontTexture::FontTexture(SDL_Renderer *r, SDL_Rect *vp):
    Texture(r, vp),
    _text(" "),
    _flashTimer(0.f),
    _useColorFlash(true)
{
        _font = NULL;
        _fontSize = 10;
        LoadFont();
}

FontTexture::~FontTexture(){}


void FontTexture::SetTextToRender(std::string text, SDL_Color textColor)
{
    //Render text
    if( !LoadFromRenderedText( text, textColor ) )
    {
        std::cout << "Failed to render text texture!\n";
    }
}

void FontTexture::SetTextToRender(std::string text)
{
    //Render text
    if( !LoadFromRenderedText( text, _fontColor ) )
    {
        std::cout << "Failed to render text texture!\n";
    }
}

bool FontTexture::LoadFromRenderedText( std::string textureText, SDL_Color textColor )
{
    //store the text
    _text = textureText;

	//Get rid of preexisting texture
	Free();

	//Render text surface
	SDL_Surface* textSurface = TTF_RenderText_Solid( _font, _text.c_str(), textColor );
	if( textSurface == NULL )
	{
		std::cout << "Unable to render text surface! SDL_ttf Error: " << TTF_GetError()  << "\n";
	}
	else
	{
		//Create texture from surface pixels
        _texture = SDL_CreateTextureFromSurface( _renderer, textSurface );
		if( _texture == NULL )
		{
			std::cout << "Unable to create texture from rendered text! SDL Error: " << SDL_GetError() << "\n";
		}
		else
		{
			//Get image dimensions
			_width = textSurface->w;
			_height = textSurface->h;
		}

		//Get rid of old surface
		SDL_FreeSurface( textSurface );
	}

	//Return success
	return _texture != NULL;
}

void FontTexture::SetFontSize(int i)
{
    _fontSize = i;
    LoadFont();
}

void FontTexture::SetFontColor(int r, int g, int b)
{
    _fontColor.r = r;
    _fontColor.g = g;
    _fontColor.b = b;
}

void FontTexture::SetFontColor(SDL_Color c)
{
    _fontColor = c;
}

void FontTexture::LoadFont()
{
	//Open the font
	_font = TTF_OpenFont( "./assets/fonts/PressStart2P.ttf", _fontSize );
	if( _font == NULL )
	{
		std::cout << "Failed to load lazy font! SDL_ttf Error: " << TTF_GetError()  << "\n";
	}
}

void FontTexture::FlashFont(float dt, float interval)
{
    _flashTimer += dt;
    if(_flashTimer >= interval)
    {
        _flashTimer = 0.f;
        _useColorFlash = !_useColorFlash;
    }

    SDL_Color c = { 0, 0 ,0};
    if(_useColorFlash)
    {
        c = _fontColor;
    }

    //reload the font with the new color
    LoadFromRenderedText(_text, c);
}

