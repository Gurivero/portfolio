#include "Button.h"

Button::Button(int x, int y , int w, int h):
    _x(x),
    _y(y),
    _width(w),
    _height(h)
{
    //ctor
}

Button::Button():
    _x(-1),
    _y(-1),
    _width(-1),
    _height(-1)
{}

Button::~Button()
{
    //dtor
}

//handle input events.
void Button::HandleEvent( SDL_Event* e)
{
    if(MouseOver())
    {
        switch(e->type)
        {
        case SDL_MOUSEBUTTONDOWN:
            CallBack();
            break;
        default:

            break;
        }
    }
}

void Button::SetClickCallback(void(*method)(void *obj), void *o)
{
    _obj = o;
    click = method;
}

void Button::CallBack()
{
    click(_obj);
}

//Check if mouse is over this object
bool Button::MouseOver()
{
    int x = inputManager::instance()->MousePointer()->first;
    int y = inputManager::instance()->MousePointer()->second;

    //check if it's inside X bounds
    if(x >= _x && x <= _x + _width)
    {
        //if inside Y bounds then the mouse is over this object
        return y >= _y && y <= _y + _height;
    }


    return false;
}

void Button::SetButtonProps(int x, int y, int w, int h)
{
   _x = x;
   _y = y;
   _width = w;
   _height = h;
}

