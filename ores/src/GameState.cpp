#include "GameState.h"
#include <Texture.h>
#include <GameUI.h>
#include <StateManager.h>
#include <MusicManager.h>

typedef Singleton< StateManager > stateManager;
typedef Singleton< MusicManager > musicManager;

GameState::GameState(SDL_Renderer *r):
    State(r),
    _timer(0.f),
    _score(0),
    _grid(this),
    _ui(this),
    _phase(PREGAME),
    _flashValue(0.f),
    _flashDir(1),
    _flashTarget(1.f)
{
    _gameVPort.x = 0;
    _gameVPort.y = 0;
    _gameVPort.w = SCREEN_WIDTH;
    _gameVPort.h = SCREEN_HEIGHT;

    //initialize the game grid
    _grid.InitGrid();

}

GameState::~GameState()
{
    //dtor
}

GameUI* GameState::GetUI()
{
    return &_ui;
}

SDL_Rect* GameState::GameViewPort()
{
    return &_gameVPort;
}

void GameState::UpdateContents(float dt)
{
    if(_phase == GAME && _grid.InDanger())
    {
        //flash screen between black and red
        FlashScreen(dt);
    }
    else
    {
        //Clear screen
        SDL_SetRenderDrawColor( _renderer, 0x00, 0x00, 0x00, 0xFF );
    }

    SDL_RenderClear( _renderer );

    //Render the Game viewport
    SDL_RenderSetViewport( _renderer, &_gameVPort );

    //Update and render the grid contents
    UpdateGame();

    //update the game ui
    _ui.Update(dt);

    //Update screen
    SDL_RenderPresent( _renderer );
 }

 void GameState::UpdateGame()
 {
    if(_phase != GAMEOVER)
    {
        _grid.Update();
    }
 }

 void GameState::Start()
 {
    _timer = 0.f;
    _grid.InitialFill();
 }

 void GameState::Stop()
 {
 }

 void GameState::LostGame()
 {
     _phase = GAMEOVER;
     musicManager::instance()->StopMusic();
     _timer = 0.f;
 }


 void GameState::Update(float dt)
 {
     _timer += dt;

     switch(_phase)
     {
     case PREGAME: //initial messages

         if(_timer > 3.f)
        {
            _phase = GAME;
            _timer = 0.f;

        }
        break;
     case GAME: //normal game time

        if(_timer >= TIME_ADVANCE_GRID)
        {
            //advance the grid
            _grid.AdvanceGrid();
            _timer = 0.f;
        }
        break;
     case GAMEOVER: //game over message

         //show game over for 3 seconds and go to menu
         if(_timer > 2.f)
         {
            stateManager::instance()->ChangeState(StateManager::MENU);
         }
        break;
     default:
        break;
     }

    UpdateContents(dt);
 }

void GameState::CheckCubeDel(Cube *c)
{
    _grid.CheckCubeDel(c);
}

float GameState::GridTimer()
{
    return _timer;
}

int GameState::Score()
{
    return _score;
}

void GameState::IncrScore(int i)
{
    _score += i;
}

GameState::GAME_PHASE GameState::Phase()
{
    return _phase;
}

//flash screen when the first column is not empty
void GameState::FlashScreen(float dt)
{
    _flashValue += _flashDir * dt * FLASH_SCREEN_SPEED ;
    if(_flashDir == -1 && _flashValue <= 0.f)
    {
        _flashValue = 0.f;
        _flashDir = 1;
    }
    else if(_flashDir == 1 && _flashValue >= 100.f)
    {
        _flashValue = 100.f;
        _flashDir = -1;
    }
    SDL_SetRenderDrawColor( _renderer, _flashValue, 0x00, 0x00, 0xFF );
}
