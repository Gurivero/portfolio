#include "MenuState.h"
#include <StateManager.h>
#include <MusicManager.h>

typedef Singleton< StateManager > sm;
typedef Singleton< MusicManager> musicManager;

MenuState::MenuState(SDL_Renderer *r):
    State(r),
    _timer(0.f),
    _ui(this),
    _startButton(0,0,SCREEN_WIDTH,SCREEN_HEIGHT)
{
    _viewPort.x = 0;
    _viewPort.y = 0;
    _viewPort.w = SCREEN_WIDTH;
    _viewPort.h = SCREEN_HEIGHT;

    //when clicking on screen the game will go to the game state
    _startButton.SetClickCallback(&GoToGame, (void*)this);
    musicManager::instance()->PlayMusic("./assets/sound/music/game_music.mp3");
}

MenuState::~MenuState()
{
    //dtor
}

SDL_Rect* MenuState::ViewPort()
{
    return &_viewPort;
}

void MenuState::GoToGame(void *o)
{
    //callback to click on menu screen
    sm::instance()->ChangeState(StateManager::GAME);
}

void MenuState::Start()
{
    inputManager::instance()->AddListener(&_startButton);
}

void MenuState::Stop()
{
    inputManager::instance()->RemListener(&_startButton);
}

void MenuState::Update(float dt)
{
    UpdateContents(dt);
}

void MenuState::UpdateContents(float dt)
{
    //Clear screen
    SDL_SetRenderDrawColor( _renderer, 0x00, 0x00, 0x00, 0xFF );
    SDL_RenderClear( _renderer );

    //Render the Game viewport
    SDL_RenderSetViewport( _renderer, &_viewPort );

    //update the game ui
    _ui.Update(dt);

    //Update screen
    SDL_RenderPresent( _renderer );
}
