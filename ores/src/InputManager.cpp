#include "InputManager.h"
#include <GameState.h>

typedef std::pair< Clickable*, Clickable* > mapEntry;

InputManager::InputManager():
    _quitVar(NULL),
    _mousePointer(),
    _cMap(),
    _toRem()
{
    //ctor
}

InputManager::~InputManager()
{
    //dtor
}

void InputManager::Update(float dt)
{
    //remove all flagged clickables
    RemFlaggedListeners();

    //update mouse pointer position
    SDL_GetMouseState( &_mousePointer.first, &_mousePointer.second );

    //loop through all input events
    while( SDL_PollEvent( &_event ) != 0)
    {
        //quit application request
        if( _event.type == SDL_QUIT)
        {
            *_quitVar = true;
        }

        clickMap::iterator it;
        mapEntry me;

        switch(_event.type)
        {

        //check mouse button presses
        case SDL_MOUSEBUTTONDOWN:

            for (it = _cMap.begin(); it != _cMap.end(); ++it)
            {
                me = (*it);
                me.first->HandleEvent(&_event);
            }
            break;

        default:
            break;
        }
    }
}

void InputManager::PassQuitFlag(bool *quit)
{
    _quitVar = quit;
}

void InputManager::AddListener(Clickable *c)
{
    //when adding a listener verify if the element is marked for removal
    //if so, remove it from the map
    clickMap::iterator it = _toRem.find(c);
    if(it != _toRem.end())
    {
        _toRem.erase(it);
    }

    //add the element to the listeners map
    mapEntry me(c, c);
    _cMap.insert(me);
}

void InputManager::RemListener(Clickable *c)
{
    mapEntry me(c,c);

    //mark the cube to removal
    _toRem.insert(me);
}

void InputManager::RemFlaggedListeners()
{
    clickMap::iterator it;
    mapEntry me;

    //remove the flagged objects
    for(it = _toRem.begin(); it != _toRem.end(); ++it)
    {
        me = *it;
        _cMap.erase(me.first);
    }

    //clear the map of flags
    _toRem.clear();
}

std::pair< int, int >* InputManager::MousePointer()
{
    return &_mousePointer;
}

void InputManager::SetState(State *s)
{
    _state = s;
}

void InputManager::Clear()
{
    _cMap.clear();
    _toRem.clear();
}
