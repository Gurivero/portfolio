#include "Texture.h"
#include <iostream>
#include <SDL_image.h>

Texture::Texture(SDL_Renderer *r, SDL_Rect *vp)
{
    _viewport = vp;
    _renderer = r;
    _texture = NULL;
    _width = 0;
    _height = 0;

}

Texture::~Texture()
{
	//Deallocate
	Free();
}

bool Texture::LoadFromFile( std::string path )
{
	//Get rid of preexisting texture
	Free();

	//The final texture
	SDL_Texture* newTexture = NULL;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load( path.c_str() );
	if( loadedSurface == NULL )
	{
		std::cout << "Unable to load image %s! SDL_image Error: " << path.c_str() << " " << IMG_GetError() << "\n";
	}
	else
	{
		//Create texture from surface pixels
        newTexture = SDL_CreateTextureFromSurface( _renderer, loadedSurface );
		if( newTexture == NULL )
		{
			std::cout << "Unable to create texture from %s! SDL Error: " << path.c_str() << "" << SDL_GetError() ;
		}
		else
		{
			//Get image dimensions
			_width = loadedSurface->w;
			_height = loadedSurface->h;

            //simple (but WRONG, it causes stretching!)
            //way to clamp texture to viewport

            if(_width > _viewport->w)
            {
                _width = _viewport->w;
            }
            if(_height > _viewport->h)
            {
                _height = _viewport->h;
            }
		}

		//Get rid of old loaded surface
		SDL_FreeSurface( loadedSurface );
	}

	//Return success
	_texture = newTexture;
	return _texture != NULL;
}

void Texture::Free()
{
	//Free texture if it exists
	if(_texture)
	{
	     SDL_DestroyTexture( _texture );
        _texture = NULL;
        _width = 0;
        _height = 0;
	}

}

void Texture::Render( int x, int y )
{
    //Set rendering space and render to screen
    SDL_Rect renderQuad = { x, y, _width, _height };
    Render(&renderQuad);
}

void Texture::Render( int x, int y, int w, int h )
{
    //Set rendering space and render to screen
    SDL_Rect renderQuad = { x, y, w, h };
    Render(&renderQuad);
}

void Texture::Render( SDL_Rect *renderQuad )
{
    //setColor( r, g, b );
	SDL_RenderCopy( _renderer, _texture, NULL, renderQuad );
}

int Texture::Width()
{
	return _width;
}

int Texture::Height()
{
	return _height;
}

void Texture::SetColor(int r, int g, int b)
{
    SDL_SetTextureColorMod(_texture, r, g, b);
}

void Texture::SetAlpha(int a)
{
    SDL_SetTextureAlphaMod( _texture, a);
}
