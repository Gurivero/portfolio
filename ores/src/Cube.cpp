#include "Cube.h"
#include <stdio.h>
#include <stdlib.h>
#include <GameState.h>
#include <InputManager.h>

Cube::Cube() :
    _texture(NULL),
    _renderer(NULL),
    _viewPort(NULL),
    _color(NONE),
    _x(-1),
    _y(-1),
    _cubeSize(-1),
    _destroy(false)
{
    //setting the click callback
   // _button.SetClickCallback(&ClickCube, this);
}

Cube::~Cube()
{
    Clear();
}

void Cube::AddColor(CUBE_COLOR c)
{
    if(c == NONE)
    {
        //choose a random color for the cube
        int r = rand() % NUM_COLORS;
        _color = static_cast< CUBE_COLOR >(r);
    }
    else
    {
        //clear the old texture;
        if(_texture)
        {
            Clear();
        }
        _color = c;
    }

    //load the new cube texture
    _texture = new Texture(_renderer, _viewPort);
    _texture->LoadFromFile("./assets/textures/" + GetTexPath() +".png");

}

std::string Cube::GetTexPath()
{
    switch(_color)
    {
    case RED:
        return "red";
    case BLUE:
        return "blue";
    case GREEN:
        return "green";
    case ORANGE:
        return "orange";
    case PURPLE:
        return "purple";
    default:
        return "";
    }
}

void Cube::Update()
{
    //cube is visible, so render it
    if(_texture)
    {
        //render the cube
        _texture->Render(_x,_y, _cubeSize, _cubeSize);

        //draw an overlay over the cube under the mouse pointer
        if(MouseOver())
        {
            Texture *mouse = _gs->GetUI()->MouseOverTex();
            mouse->Render(_x,_y, _cubeSize, _cubeSize);
        }
    }
}

bool Cube::MouseOver()
{
    int x = inputManager::instance()->MousePointer()->first;
    int y = inputManager::instance()->MousePointer()->second;

    //check if it's inside X bounds
    if(x >= _x && x <= _x + _cubeSize)
    {
        //if inside Y bounds then the mouse is over this object
        return y >= _y && y <= _y + _cubeSize;
    }

    return false;
}

void Cube::Clear()
{
    if(_texture)
    {
        _texture->Free();
        free(_texture);
        _texture = NULL;
    }
}

bool Cube::IsEmpty()
{
    return _color == NONE;
}

void Cube::SetCubeCoords()
{
    int cidx = _col;

    //cube size relative to the height of the window
    int maxHeight = _viewPort->h * 7/10;

    //define a size for each and every cube
    _cubeSize = maxHeight / GRID_ROW_NUM;

    //cube coordinates relative to the viewport window
    _x = _viewPort->w - ((GRID_COL_NUM - cidx) * _cubeSize);
    _y = (_viewPort->h * 9/10) - _row * _cubeSize - _cubeSize;
}

//handle input events.
void Cube::HandleEvent( SDL_Event* e)
{
    if(!IsEmpty() && MouseOver())
    {
        switch(e->type)
        {
        case SDL_MOUSEBUTTONDOWN:
             _gs->CheckCubeDel(this);
            break;
        default:

            break;
        }
    }
}

//void Cube::ClickCube(void *obj)
//{
////    static_cast<Cube>(obj)->CheckCubeDel();
//}
//
//void Cube::CheckCubeDel()
//{
//    //if this cube was clicked, check the conditions
//    //for destruction
//    _gs->CheckCubeDel(this);
//}

Cube::CUBE_COLOR Cube::Color()
{
    return _color;
}

bool Cube::MarkedToDestroy()
{
    return _destroy;
}

void Cube::MarkToDestroy()
{
    _destroy = true;
}

void Cube::Destroy()
{
    //only destroy the cube if it's visible
    if(!IsEmpty())
    {
        //clear the texture
        Clear();
        _color = NONE;
        _destroy = false;

        //remove the cube from the map of input listeners
        inputManager::instance()->RemListener(this);
    }
}

Texture* Cube::GetTexture()
{
    return _texture;
}

void Cube::Init(int c, int r, GameState *gs)
{
    _col = c;
    _row = r;
    _gs = gs;
    _renderer = _gs->Renderer();
    _viewPort = _gs->GameViewPort();

    //set the cube screen coordinates
    SetCubeCoords();
}

void Cube::FillCube(CUBE_COLOR c)
{

    //add color <c> to the cube
    AddColor(c);

    //set button location and size
    //_button.SetButtonProps(_x, _y, _cubeSize, _cubeSize);

    //cube listens to mouse clicks on it
    inputManager::instance()->AddListener(this);
}
