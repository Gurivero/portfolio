﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	public bool is_against_ai;
	public bool is_sandbox;
	public bool skip_tutorial;

	private bool _pause_game;

	public enum EGAME_PHASE
	{
		MENU,
		PREMATCH,
		PREROUND,
		MATCH,
		POSTROUND,
		POSTMATCH
	}

	public enum EGAME_MODE
	{
		MATCH,
		FFA,
		//FREEZE,
		VOLLEY,
		DODGEBALL,
		TOURNAMENT,
		CHALLENGE
	}

	public enum EMENU_PHASES
	{
		MAIN_MENU,
		NUM_PLAYERS,
		GAME_MODE,
		CHOOSE_MAP,
		SETTINGS
	}

	public GameObject net_prefab;
	public GameObject ball_hit_prefab;
	public float ball_timer_value;
	public float round_wait_timer_value;
	public int max_goals;
	public Color[] player_colors;
	
	EGAME_MODE _match_game_mode;
	int _match_num_players;
//	bool _match_against_ai;
	string _match_map;

	BaseGameMode _game_mode;
	UIManager _ui;

	EGAME_PHASE _phase;
	EMENU_PHASES _menu_phase;
	

	public EGAME_MODE match_game_mode
	{
		get
		{
			return _match_game_mode;
		}
		set
		{
			_match_game_mode = value;
		}
	}
	
	public int match_num_players
	{
		get
		{
			return _match_num_players;
		}
		set
		{
			_match_num_players = value;
		}
	}
	
	public string match_map
	{
		get
		{
			return _match_map;
		}
		set
		{
			_match_map = value;
		}
	}

	public bool pause_game
	{
		get{
			return _pause_game;
		}
		set{
			_pause_game = value;
			PauseGame();
		}
	}

	void Awake()
	{
	}

	// Use this for initialization
	void Start () {
		_ui = MasterManager.instance.ui_manager;
	}
	
	// Update is called once per frame
	void Update () {
		switch(_phase)
		{
		case EGAME_PHASE.MENU:
			break;
		case EGAME_PHASE.PREMATCH:
			break;
		case EGAME_PHASE.PREROUND:
			_ui.UpdatePhasePreRound();
			_game_mode.UpdatePreRound();
			break;
		case EGAME_PHASE.MATCH:
			_ui.UpdatePhaseMatch();
			_game_mode.UpdateMatch();
			break;
		case EGAME_PHASE.POSTROUND:
			_ui.UpdatePhasePostRound();
			_game_mode.UpdatePostRound();
			break;
		case EGAME_PHASE.POSTMATCH:
			break;
		default:
			Debug.Log("Error: Update ()");
			break;
		}
	}

	public void StartMatch()
	{
		StartMatch (_match_game_mode, _match_num_players, false /*_match_against_ai*/);
	}

	void StartMatch(EGAME_MODE gm, int num_players, bool against_ai)
	{
		if(GetComponent< BaseGameMode >() != null)
		{
			DestroyGameMode();
		}

		switch(gm)
		{
		case EGAME_MODE.MATCH:
			gameObject.AddComponent< MatchGameMode >();
			break;
		case EGAME_MODE.TOURNAMENT:
			gameObject.AddComponent< TournamentGameMode >();
			break;
		case EGAME_MODE.CHALLENGE:
			gameObject.AddComponent< ChallengeGameMode >();
			break;
		case EGAME_MODE.FFA:
			gameObject.AddComponent< FFAGameMode >();
			break;
		case EGAME_MODE.VOLLEY:
			gameObject.AddComponent< VolleyGameMode >();
			break;
		case EGAME_MODE.DODGEBALL:
			gameObject.AddComponent< DodgeBallGameMode >();
			break;
		default:
			Debug.Log("ERROR: void StartMatch(GAME_MODE gm)");
			break;
		}
		_game_mode = GetComponent< BaseGameMode > ();
		_game_mode.StartMatch (gm, num_players, against_ai);
	}

	public BaseBall ball
	{
		get
		{
			return _game_mode.ball;
		}
	}

	public BaseField field
	{
		get
		{
			return _game_mode.field;
		}
	}

	public EGAME_PHASE game_phase
	{
		get
		{
			return _phase;
		}
		set
		{
			_phase = value;
			switch (_phase) {
			case EGAME_PHASE.MENU:
				_ui.ShowPhaseMenu();
				break;
			case EGAME_PHASE.PREMATCH:
				_ui.ShowPhasePreMatch();
				break;
			case EGAME_PHASE.PREROUND:
				_ui.ShowPhasePreRound();
				break;
			case EGAME_PHASE.MATCH:
				_ui.ShowPhaseMatch();
				break;
			case EGAME_PHASE.POSTROUND:
				_ui.ShowPhasePostRound();
				break;
			case EGAME_PHASE.POSTMATCH:
				//_ui.ShowPhasePostMatch();
				break;
			default:
				Debug.Log("ERROR: GamePhase(GAME_PHASE gp)");
				break;
			}
		}
	}

	public EMENU_PHASES menu_phase
	{
		get
		{
			return _menu_phase;
		}
		set
		{
			_menu_phase = value;
		}
	}

	public void GoToMenu()
	{
		pause_game = false;
		game_phase = GameManager.EGAME_PHASE.MENU;
		menu_phase = EMENU_PHASES.MAIN_MENU;
		DestroyGameMode ();
		_ui.ShowMenuMain ();
	}

	public BaseGameMode game_mode
	{
		get
		{
			return _game_mode;
		}
	}

	public void DestroyGameMode()
	{
		if(_game_mode != null)
		{
			game_mode.DestroyEverything ();
			Destroy(GetComponent< BaseGameMode >());
			_game_mode = null;
		}
	}

	public string GameModeName()
	{
		switch(match_game_mode)
		{
		case EGAME_MODE.CHALLENGE:
			return "Challenge";
		case EGAME_MODE.FFA:
			return "Free-For-All";
		case EGAME_MODE.DODGEBALL:
			return "Dodgeball";
		case EGAME_MODE.MATCH:
			return "Match";
		case EGAME_MODE.TOURNAMENT:
			return "Tournament";
		case EGAME_MODE.VOLLEY:
			return "Volley";
		default:
			return "";
		}
	}

	public string GameModeDesc()
	{
		switch(match_game_mode)
		{
		case EGAME_MODE.CHALLENGE:
			return "TODO";
		case EGAME_MODE.FFA:
			return "Score " + max_goals + " goals to win the game.";
		case EGAME_MODE.DODGEBALL:
			return "Hit the opponents to score. First to " + max_goals + " goals wins.";
		case EGAME_MODE.MATCH:
			return "Score " + max_goals + " goals to win the game.";
		case EGAME_MODE.TOURNAMENT:
			return "Todo";
		case EGAME_MODE.VOLLEY:
			return "If the ball stops on your court, it's a point for the opponent! First to " + max_goals + " points wins.";
		default:
			return "";
		}
	}

	
	void PauseGame()
	{
		if (game_phase == EGAME_PHASE.MATCH) 
		{
			_ui.ShowPauseOverlay (_pause_game);
			if(_pause_game)
			{
				Time.timeScale = 0.000001f;
			}
			else
			{
				Time.timeScale = 1f;
			}
		}
		else
		{
			Time.timeScale = 1f;
			_ui.ShowPauseOverlay (false);
		}
	}
}
