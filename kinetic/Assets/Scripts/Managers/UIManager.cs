using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class UIManager : MonoBehaviour {

	public Canvas[] canvas_array;
	public Canvas[] play_canvas;
	public GameObject[] menu_canvas;
	public GameObject mid_screen_msg;
	public GameObject tutorial_overlay;
	public GameObject pause_overlay;

	ScoreBoard  _scoreboard, _p1_timer, _p2_timer;

	GameManager _game_manager;
	BaseGameMode _game_mode;

	Stack< GameManager.EMENU_PHASES > _menu_stack;


	// Use this for initialization
	void Start () {
		_menu_stack = new Stack<GameManager.EMENU_PHASES> ();
		_game_manager = MasterManager.instance.game_manager;
		EnableCanvas (GameManager.EGAME_PHASE.MENU);
		_game_manager.GoToMenu ();
	}
	
	// Update is called once per frame
	void Update () {
	}


	void EnableCanvas(GameManager.EGAME_PHASE ct)
	{
		canvas_array [0].gameObject.SetActive (false);
		canvas_array [1].gameObject.SetActive (false);
		if(ct == GameManager.EGAME_PHASE.MENU)
		{
			canvas_array [0].gameObject.SetActive (true);
		}
		else
		{
			canvas_array [1].gameObject.SetActive (true);
		}
	}


	public void ShowPhaseMenu()
	{
		EnableCanvas (GameManager.EGAME_PHASE.MENU);

	}

	public void ShowPhasePreMatch()
	{
		_game_mode = _game_manager.game_mode;
		pause_overlay.SetActive (false);
		ShowTutorial ();
		EnableCanvas (GameManager.EGAME_PHASE.MATCH);
		DisableAllPlayCanvas ();
		ShowSpecificModeUI ();
	}

	void ShowTutorial()
	{
		tutorial_overlay.SetActive (true);
		TutorialHelper th = tutorial_overlay.GetComponent< TutorialHelper > ();
		th.game_mode_name.text = _game_manager.GameModeName ();
		th.gm_description.text = _game_manager.GameModeDesc ();
	}

	public void CloseTutorial()
	{
		tutorial_overlay.SetActive (false);
		_game_manager.game_phase = GameManager.EGAME_PHASE.PREROUND;
		_game_mode.InitRound (Player.EPLAYER_ID.P1);
	}

	public void ShowPhasePostRound()
	{
		_game_mode = _game_manager.game_mode;
		mid_screen_msg.SetActive (true);
		EnableCanvas (GameManager.EGAME_PHASE.MATCH);
		DisableAllPlayCanvas ();
		ShowSpecificModeUI ();
	}

	public void ShowPhasePreRound()
	{
		_game_mode = _game_manager.game_mode;
		mid_screen_msg.SetActive (true);
		EnableCanvas (GameManager.EGAME_PHASE.MATCH);
		DisableAllPlayCanvas ();
		ShowSpecificModeUI ();
	}

	void DisableAllPlayCanvas()
	{
		foreach(Canvas c in play_canvas)
		{
			c.gameObject.SetActive(false);
		}
	}

	public void ShowPhaseMatch()
	{
		EnableCanvas (GameManager.EGAME_PHASE.MATCH);

		mid_screen_msg.SetActive (false);
	}

	void ShowModeMatch()
	{
		play_canvas [(int)GameManager.EGAME_MODE.MATCH].gameObject.SetActive(true);
		ActivateScoreBoards ();
	}

	void ActivateScoreBoards()
	{
		int idx = (int)_game_mode.game_mode_type;

		int num_players = _game_mode.NumPlayers () - 2;
		
		GameObject[] scores = play_canvas[idx].GetComponent< CanvasAux > ().players_scoreboards;
		
		foreach(GameObject g in scores)
		{
			g.SetActive(false);
		}

		_scoreboard = scores [num_players].GetComponent< ScoreBoard > ();
		_scoreboard.gameObject.SetActive (true);
	}

	void ShowModeFFA()
	{
		play_canvas[(int)GameManager.EGAME_MODE.FFA].gameObject.SetActive(true);
		ActivateScoreBoards ();
	}

	public void UpdatePhasePostRound()
	{
		string msg = "GOAL!";
		mid_screen_msg.GetComponentInChildren< Text >().text = msg;
		_scoreboard.UpdateScoreboard ();
	}

	public void UpdatePhasePreRound()
	{
		string msg = "";
		if(_game_mode.RoundWaitTimer() > 1f)
		{
			msg = "READY";
		}
		else
		{
			msg = "GO!";
		}
		mid_screen_msg.GetComponentInChildren< Text >().text = msg;
		_scoreboard.UpdateScoreboard ();
	}


	void ShowSpecificModeUI()
	{
		switch(_game_mode.game_mode_type)
		{
		case GameManager.EGAME_MODE.FFA:
			ShowModeFFA();
			break;
		case GameManager.EGAME_MODE.MATCH:
		case GameManager.EGAME_MODE.TOURNAMENT:
		//case GameManager.EGAME_MODE.FREEZE:
		case GameManager.EGAME_MODE.VOLLEY:
		case GameManager.EGAME_MODE.DODGEBALL:
			ShowModeMatch();
			break;
		default:
			Debug.Log("ERROR: ShowSpecificModeUI()");
			break;
		}
	}

	public void UpdatePhaseMatch()
	{
		_scoreboard.UpdateScoreboard ();
		_scoreboard.UpdateBallTimer ();

	}
	
	public void ShowPauseOverlay(bool show)
	{
		pause_overlay.SetActive (show);
	}

	void HideAllMenus()
	{
		foreach(GameObject c in menu_canvas)
		{
			c.SetActive(false);
		}
	}

	void ShowMenu(int i, bool goingback = false)
	{
		if (!goingback) {
			_menu_stack.Push (_game_manager.menu_phase);
		}
		_game_manager.menu_phase = (GameManager.EMENU_PHASES) i;

		HideAllMenus ();
		 
		menu_canvas [i].SetActive (true);
	}

	public void ShowLastMenu()
	{
		GameManager.EMENU_PHASES menu = _menu_stack.Pop ();
		ShowMenu ((int)menu, true);
	}

	public void ShowMenuMain()
	{
		ShowMenu ((int)GameManager.EMENU_PHASES.MAIN_MENU);
	}

	public void ShowMenuNumPlayers()
	{
		ShowMenu ((int)GameManager.EMENU_PHASES.NUM_PLAYERS);
	}
	
	public void ShowMenuGameMode(int num_players)
	{
		_game_manager.match_num_players = num_players;
		ShowMenu ((int)GameManager.EMENU_PHASES.GAME_MODE);
	}

	void StartMatch()
	{
		HideAllMenus ();
		_menu_stack.Clear ();
		_game_manager.StartMatch ();
	}

	public void ShowMenuChooseMap(int gamemode)
	{
		_game_manager.match_game_mode = (GameManager.EGAME_MODE) gamemode;
		StartMatch ();
		//_game_manager.menu_phase = GameManager.EMENU_PHASES.CHOOSE_MAP;
	}

	public void ShowMenuSettings()
	{
		ShowMenu ((int)GameManager.EMENU_PHASES.SETTINGS);
	}
}
