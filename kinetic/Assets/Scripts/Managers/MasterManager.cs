using UnityEngine;
using System.Collections;

public class MasterManager : MonoBehaviour {

	GameManager _game_manager;
	ControllerManager _controller_manager;
	UIManager _ui_manager;
	SoundManager _sound_manager;

	public static MasterManager instance;

	void Awake()
	{
		_game_manager = GetComponentInChildren< GameManager > ();
		_controller_manager = GetComponentInChildren< ControllerManager > ();
		_ui_manager = GetComponentInChildren< UIManager > ();
		_sound_manager = GetComponentInChildren< SoundManager > ();
		instance = this;
	}

	public BaseGameMode game_mode
	{
		get
		{
			return _game_manager.game_mode;
		}
	}

	public GameManager.EGAME_MODE GameModeType()
	{
		return _game_manager.game_mode.game_mode_type;
	}

	public GameManager game_manager
	{
		get
		{
			return _game_manager;
		}
	}

	public ControllerManager controller_manager
	{
		get
		{
			return _controller_manager;
		}
	}

	public UIManager ui_manager
	{
		get
		{
			return _ui_manager;
		}
	}

	public SoundManager sound_manager
	{
		get
		{
			return _sound_manager;
		}
	}
}
