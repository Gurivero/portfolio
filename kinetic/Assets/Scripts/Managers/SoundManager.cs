﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SoundManager : MonoBehaviour {

	public enum ESOUNDS
	{
		SHOOT,
		WALL_HIT
	}

	public enum EMUSIC
	{
		THEME
	}

	public AudioClip[] sounds;
	public AudioClip[] musics;

	AudioSource _music;

	int _mute_sound;
	int _mute_music;

	// Use this for initialization
	void Start () {
		_mute_sound = PlayerPrefs.GetInt ("mute_sound", 0);
		_mute_music = PlayerPrefs.GetInt ("mute_music", 0);
		if(_mute_music == 0)
		{
			PlayMusic (EMUSIC.THEME);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void PlayMusic(EMUSIC m)
	{
		if (_mute_music == 0)
		{
			_music = LeanAudio.playClipAt (musics [(int)m], Vector3.zero, true);
			_music.gameObject.name = "MusicGO";
		}
	}

	public void PlaySound(ESOUNDS e)
	{
		if(mute_sound == 0)
			LeanAudio.playClipAt (sounds[(int)e], Vector3.zero);
	}

	public int mute_sound
	{
		get
		{
			return _mute_sound;
		}
		set
		{
			_mute_sound = value;
			PlayerPrefs.SetInt("mute_sound", _mute_sound);
		}
	}

	public int mute_music
	{
		get
		{
			return _mute_music;
		}
		set
		{
			_mute_music = value;
			PlayerPrefs.SetInt("mute_music", _mute_music);
		}
	}

	public string GetSoundButtonText()
	{
		if(mute_sound == 1)
		{
			return "Sound : Off";
		}
		else
		{
			return "Sound : On";
		}
	}

	public void ToggleSound(Text t)
	{
		mute_sound = mute_sound == 1 ? 0 : 1;
		t.text = GetSoundButtonText ();

	}
	public void ToggleMusic(Text t)
	{
		mute_music = mute_music == 1 ? 0 : 1;
		if(mute_music == 1)
		{
			t.text = GetMusicButtonText();
			_music.Stop();
			Destroy(_music.gameObject);
		}
		else
		{
			t.text = GetMusicButtonText();
			PlayMusic(EMUSIC.THEME);
		}
	}

	public string GetMusicButtonText()
	{
		if(mute_music == 1)
		{
			return "Music : Off";
		}
		else
		{
			return "Music : On";
		}
	}
}
