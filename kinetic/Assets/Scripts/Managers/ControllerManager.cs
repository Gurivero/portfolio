using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ControllerManager : MonoBehaviour {

	public static int MAX_GAMEPADS = 4;

	[SerializeField]
	private ECONTROL_TYPE _control;

	[SerializeField]
	private float _double_click_time;
//	private Dictionary< string, float > _click_time;
//	private List< string > _click_frame;



	public enum ECONTROL_TYPE
	{
		FREE,
		POSSESSION
	}

	public enum ECONTROLLER_ID
	{
		J1,
		J2,
		J3,
		J4,
		K1,
		K2
	}

	public bool force_control_type;

	public ECONTROL_TYPE control_type
	{
		get
		{
			return _control;
		}
		set
		{
			if(!force_control_type)
			{
				_control = value;
			}
		}
	}

	int _available_pads;
	int _available_keyboard;
	int _controller_count;

	void Awake()
	{
		_available_pads = 0;
//		_click_time = new Dictionary<string, float> ();
//		_click_frame = new List<string> ();
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	}

	void LateUpdate()
	{
//		foreach(string s in _click_frame)
//		{
//			_click_time.Remove(s);
//		}
//		_click_frame.Clear ();	
	}

	public bool GetButton(ECONTROLLER_ID p, string bname)
	{
		string buttonrealname = p.ToString () + bname;
		return Input.GetButton (buttonrealname);
	}

	public bool GetButtonDown(ECONTROLLER_ID p, string bname)
	{
		string buttonrealname = p.ToString () + bname;

		bool pressed = Input.GetButtonDown (buttonrealname);

		return pressed;
	}

	public bool GetButtonUp(ECONTROLLER_ID p, string bname)
	{
		string buttonrealname = p.ToString () + bname;

		bool pressed = Input.GetButtonUp (buttonrealname);

		return pressed;
	}

	public float GetAxis(ECONTROLLER_ID p, string axis)
	{
		string axisrealname = p.ToString () + axis;
		return Input.GetAxis (axisrealname);
	}

	public void ScanControllers(int num_players)
	{
		_controller_count = 0;
		_available_pads = Input.GetJoystickNames ().Length;
		_available_keyboard = 2;
	}

	public void AssignController(Player p)
	{
		if(_available_pads > 0)
		{
			p.controller = (ECONTROLLER_ID)_controller_count;
			--_available_pads;
		}
		else
		{
			if(_available_keyboard > 0)
			{
				p.controller =  (ECONTROLLER_ID) (MAX_GAMEPADS + 2 - _available_keyboard);
				--_available_keyboard;
			}
			else
			{
				return;
			}
		}
		++_controller_count;
	}
}
