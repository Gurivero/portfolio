using UnityEngine;
using System.Collections;

public class FFAGameMode : BaseGameMode {

	Player.EPLAYER_ID _last_scorer;

	public override void ScoreGoal(Player.EPLAYER_ID goal_scorer, Player.ETEAM_ID where_ball_entered, int val)
	{
		_last_scorer = goal_scorer;
		_goals[(int)goal_scorer] += val;
		ShowGoalMessage ();
	}

	protected override bool CheckEndMatch()
	{
		if(_goals[(int) _last_scorer] >= _game_manager.max_goals)
		{
			EndGame(_last_scorer);
			return true;
		}
		else
		{
			InitRound(_last_scorer);
			return false;
		}
	}

	protected override void SpecificMatchConditions()
	{
		MasterManager.instance.controller_manager.control_type = ControllerManager.ECONTROL_TYPE.POSSESSION;
		_goals = new int[_players.Length];
	}

	protected override void SpecificRoundConditions(Player.EPLAYER_ID id)
	{
		_round_wait_timer = _game_manager.round_wait_timer_value;
		Vector3 temp = _field.transform.position;
		temp.y = 5f;
		_ball.transform.position = temp;
		_ball.GetComponent< Rigidbody >().constraints = RigidbodyConstraints.None;
	}
}
