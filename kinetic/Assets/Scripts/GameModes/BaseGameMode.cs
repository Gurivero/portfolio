using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BaseGameMode : MonoBehaviour {
	
	protected BaseField _field;
	protected BaseBall _ball;
	protected float _round_wait_timer;

	protected float _ball_timer;
	
	protected Player.EPLAYER_ID _player_with_ball;
	protected Player[] _players;

	protected int[] _goals;
	protected GameManager _game_manager;
	protected GameManager.EGAME_MODE _game_mode;

	protected Player.ETEAM_ID _last_scorer_team;

	// Use this for initialization
	public void Start () {
	}

	// Update is called once per frame
	public void Update () {
	}

	public virtual void ScoreGoal(Player.EPLAYER_ID goal_scorer, Player.ETEAM_ID where_ball_entered, int val)
	{
		_last_scorer_team = where_ball_entered == Player.ETEAM_ID.TEAM_1 ? Player.ETEAM_ID.TEAM_2 : Player.ETEAM_ID.TEAM_1;
		_goals [(int)_last_scorer_team] += val;
		ShowGoalMessage ();
	}

	protected void ShowGoalMessage()
	{
		_round_wait_timer = _game_manager.round_wait_timer_value;
		_game_manager.game_phase = GameManager.EGAME_PHASE.POSTROUND;
	}


	protected void LoadPlayersAtInitPosition()
	{
		Vector3 field_pos = _field.transform.position;
		Vector3 player_pos;
		for(int i = 0 ; i < _players.Length ; ++i)
		{
			player_pos = _field.player_init_positions[i];
			if(_players.Length == 2)
			{
				player_pos.z = 0f;
			}
			_players[i].transform.position = player_pos;
			_players[i].transform.LookAt(new Vector3(field_pos.x, _players[i].transform.position.y, field_pos.z));
			_players[i].Reset();
		}
	}

	void LoadRound(Player.EPLAYER_ID id)
	{	
		LoadPlayersAtInitPosition ();
	}

	public virtual void InitRound(Player.EPLAYER_ID id)
	{
		LoadBall();

		foreach(Player pc in _players)
		{
			pc.StartMatch();
		}
		
		LoadPlayersAtInitPosition ();

		SpecificRoundConditions (id);
		
		_game_manager.game_phase = GameManager.EGAME_PHASE.PREROUND;
	}

	protected virtual void SpecificRoundConditions(Player.EPLAYER_ID id)
	{
		_players[(int)id].PossessionOnInit();
		_round_wait_timer = _game_manager.round_wait_timer_value;
	}

	public void StartMatch(GameManager.EGAME_MODE gm, int num_players, bool against_ai)
	{
		_game_mode = gm;
		_game_manager = MasterManager.instance.game_manager;
		_goals = new int[(int)Player.ETEAM_ID.COUNT];
		ResetMatch ();

		LoadField();
		LoadPlayers(num_players, against_ai);

		SpecificMatchConditions ();

		LoadRound (Player.EPLAYER_ID.P1);
		if(_game_manager.skip_tutorial)
		{
			InitRound(Player.EPLAYER_ID.P1);
		}
		else
		{
			_game_manager.game_phase = GameManager.EGAME_PHASE.PREMATCH;
		}
	}

	protected virtual void SpecificMatchConditions()
	{
		MasterManager.instance.controller_manager.control_type = ControllerManager.ECONTROL_TYPE.POSSESSION;
	}

	protected IEnumerator RoundInitWait()
	{
		yield return new WaitForSeconds(2f);
		_game_manager.game_phase = GameManager.EGAME_PHASE.MATCH;
	}

	protected virtual bool CheckEndMatch()
	{
		if(_goals[(int)Player.ETEAM_ID.TEAM_1] >= _game_manager.max_goals)
		{
			EndGame(Player.ETEAM_ID.TEAM_1);
			return true;
		}
		else if(_goals[(int)Player.ETEAM_ID.TEAM_2] >= _game_manager.max_goals)
		{
			EndGame(Player.ETEAM_ID.TEAM_2);
			return true;
		}

		Player.EPLAYER_ID idp = Player.EPLAYER_ID.P1;
		if(_last_scorer_team == Player.ETEAM_ID.TEAM_1)
		{
			idp = Player.EPLAYER_ID.P2;
		}
		InitRound(idp);
		return false;
	}
	
	protected virtual void EndGame(Player.ETEAM_ID winner_team)
	{
		_game_manager.GoToMenu ();
	}

	protected virtual void EndGame(Player.EPLAYER_ID winner)
	{
		_game_manager.GoToMenu ();
	}
	
	public void DestroyEverything()
	{
		GameObject.Destroy (_field.gameObject);
		foreach(Player p in _players)
		{
			GameObject.Destroy(p.gameObject);
		}
		GameObject.Destroy (_ball.gameObject);
	}

	protected void LoadField()
	{
		GameObject field_prefab = Resources.Load("Prefabs/fields/field") as GameObject; 
		_field = GameObject.Instantiate(field_prefab).GetComponent< BaseField >();
		_field.transform.FindChild ("sandbox").gameObject.SetActive (_game_manager.is_sandbox);
	}
	
	protected void LoadPlayers(int num_players, bool against_ai)
	{
		MasterManager.instance.controller_manager.ScanControllers (num_players);
		GameObject player_prefab = Resources.Load("Prefabs/players/player") as GameObject;
		_players = new Player[num_players];
		Vector3 field_pos = _field.transform.position;
		for(int i = 0 ; i < _players.Length ; ++i)
		{
			_players[i] = GameObject.Instantiate(player_prefab).GetComponent< Player >();
			_players[i].SetID((Player.EPLAYER_ID)i );
			if( i % 2f == 0f)
			{
				_players[i].SetTeamID( Player.ETEAM_ID.TEAM_1);
			}
			else
			{
				_players[i].SetTeamID( Player.ETEAM_ID.TEAM_2 );
				if(against_ai)
				{
					_players[i].InitAI(BaseAI.AI_TYPE.ATTACKER);
				}
			}
			_players[i].transform.position = _field.player_init_positions[i];
			_players[i].name = _players[i].id.ToString();
			_players[i].transform.LookAt(new Vector3(field_pos.x, _players[i].transform.position.y, field_pos.z));
			MasterManager.instance.controller_manager.AssignController(_players[i]);
		}
	}

	public GameManager.EGAME_MODE game_mode_type
	{
		get {
			return _game_mode;
		}
		set{
			_game_mode = value;
		}
	}

	
	protected void LoadBall()
	{
		if(_ball == null)
		{
			GameObject ball_prefab = Resources.Load("Prefabs/balls/ball") as GameObject;
			_ball = GameObject.Instantiate(ball_prefab).GetComponent< BaseBall >();
		}
		else
		{
			Rigidbody rb = _ball.GetComponent< Rigidbody >();
			rb.constraints = RigidbodyConstraints.FreezePosition;
			rb.velocity = Vector3.zero;
			rb.angularVelocity = Vector3.zero;
		}
		_ball.GameMode (this);
		_ball.Reset ();
	}

	public virtual void UpdatePreRound()
	{
		_round_wait_timer -= Time.deltaTime;
		if(_round_wait_timer <= 0f)
		{
			_game_manager.game_phase = GameManager.EGAME_PHASE.MATCH;
		}
	}

	public virtual void UpdatePostRound()
	{
		_round_wait_timer -= Time.deltaTime;
		if(_round_wait_timer <= 0f)
		{
			CheckEndMatch();
		}
	}
	
	public float RoundWaitTimer()
	{
		return _round_wait_timer;
	}
	
	public virtual void UpdateMatch()
	{
		if(_player_with_ball != Player.EPLAYER_ID.COUNT)
		{
			_ball_timer -= Time.deltaTime;
			if(_ball_timer <= 0)
			{
				_players [(int)_player_with_ball].AutoShoot ();
			}
		}
	}
	
	public virtual void ResetMatch()
	{
		for(int i = 0 ; i < _goals.Length ; ++i)
		{
			_goals[i] = 0;
		}
		PlayerIDWithBall(Player.EPLAYER_ID.COUNT);
	}
	
	public void PlayerIDWithBall(Player.EPLAYER_ID id)
	{
		_player_with_ball = id;
		_ball_timer = _game_manager.ball_timer_value;
	}
	
	
	public Player.EPLAYER_ID PlayerIDWithBall()
	{
		return _player_with_ball;
	}

	public Player PlayerWithBall()
	{
		if(_player_with_ball == Player.EPLAYER_ID.COUNT)
		{
			return null;
		}
		else
		{
			return _players[(int)_player_with_ball];
		}
	}

	public virtual bool ConditionsForShoot(Player p)
	{
		return true;
	}

	
	public int Goals(Player.EPLAYER_ID id)
	{
		return _goals[(int)id];
	}

	public int Goals(Player.ETEAM_ID id)
	{
		return _goals[(int)id];
	}

	
	public float BallTimer(bool toInt = true)
	{
		if(toInt)
		{
			return (int)_ball_timer;
		}
		else
		{
			return _ball_timer;
		}
	}

	public int NumPlayers()
	{
		return _players.Length;
	}

	public BaseBall ball
	{
		get
		{
			return _ball;
		}
	}
	
	public BaseField field
	{
		get
		{
			return _field;
		}
	}


	public List< GoalLine > OpponentGoals(Player.ETEAM_ID id)
	{
		Player.ETEAM_ID op_id = Player.ETEAM_ID.TEAM_1;
		op_id = id == op_id ? Player.ETEAM_ID.TEAM_2 : op_id;

		return _field.GetGoals (op_id);
	}
}
