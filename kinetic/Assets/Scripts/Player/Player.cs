using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	public enum EPLAYER_ID
	{
		P1,
		P2,
		P3,
		P4,
		P5,
		P6,
		COUNT
	}

	public enum ETEAM_ID
	{
		TEAM_1,
		TEAM_2,
		COUNT
	}

	public enum EPLAYER_STATUS
	{
		NONE,
		DASHING,
		JUMPING,
		STUMBLING
	}

	public enum ESHOOT_TYPE
	{
		NORMAL,
		CHIP_SHOT,
	}

	public GameObject FreeModeArea;

	public EPLAYER_ID id{ get; private set; }
	public ETEAM_ID teamID{ get; private set; }
	
	public float alt_click_time;
	public float run_force;
	public float jump_force_up;
	public float jump_force_front;
	public float dash_force;
	public float min_vel_dash_trail;
	public float decay_trail;
	public ForceMode dash_force_mode;
	public ForceMode shoot_force_mode;
	public float velocity_dash_stop;
	public float velocity_dash_stop_itself;
	public float ball_drible_force;
	public float ball_shoot_force;
	public float possess_cooldown_value;
	public float shot_charge_time;
	public float max_dampen_xz;
	public float max_dampen_y;
	public float kinetic_force;
	public float kinetic_timer;
	public float fake_gravity;
	public float stumble_force;
	public float stumble_shutdown;
	public float max_vel_catch_dball;
	public TrailRenderer trail_renderer;

	Vector3 _move_vector;
	Vector3 _look_dir;
	Vector3 _last_velocity;
	
	Vector2 _input_vector;
	
	float _time_pressing_shoot;
	float _possess_cooldown;
	float _stumble_time;
	float _curr_kinetic_force;

	bool _on_shot_area;
	bool _using_kinetic_control;

	EPLAYER_STATUS _status;
	ControllerManager.ECONTROLLER_ID _controller_id;

	Rigidbody _rigid_body;
	Rigidbody _ball_rb;

	Animator _animator;
	
	BaseBall _ball_script;

	BaseAI _ai;

	BaseGameMode _game_mode;

	GameManager _game_manager;

	ControllerManager _cm;

	SoundManager _sm;

	ParticleSystem _ps;


	/*

	shot y goes from -0.07 to -2.5
	shot z goes from 0 to -1

	 */

	// Use this for initialization
	void Start () {
		Reset ();
	}

	public void SetID(EPLAYER_ID i)
	{
		id = i;
	}
	public void SetTeamID(ETEAM_ID i)
	{
		teamID = i;
	}

	public void Reset()
	{
		_ps = GetComponentInChildren< ParticleSystem > ();
		_ps.startColor = MasterManager.instance.game_manager.player_colors [(int)id];
		_animator = GetComponent< Animator > ();
		_rigid_body = GetComponent< Rigidbody > ();
		_status = EPLAYER_STATUS.NONE;
		_input_vector = Vector2.zero;
		NullVelocity ();
		_game_mode = MasterManager.instance.game_mode;
		_game_manager = MasterManager.instance.game_manager;
		_cm = MasterManager.instance.controller_manager;
		_sm = MasterManager.instance.sound_manager;

		trail_renderer.time = 0f;

		InitControlMode ();
	}

	//testing two different types of control, right now only the possession mode is active
	void InitControlMode()
	{
		switch(_cm.control_type)
		{
		case ControllerManager.ECONTROL_TYPE.FREE:
			FreeModeArea.SetActive(true);
			break;
		case ControllerManager.ECONTROL_TYPE.POSSESSION:
			FreeModeArea.SetActive(false);
			break;
		default:
			break;
		}
	}

	public bool HasBall()
	{
		bool ball_in_feet = _ball_rb.transform.parent == transform;

		return ball_in_feet || _on_shot_area;
	}

	void OnTriggerEnter(Collider other) 
	{
		if (other.tag.Equals("Ball") && !IsPossessing())
		{
			BallCollisionBehaviour(other);
		}
	}

	void OnTriggerStay(Collider other) 
	{
		switch(_cm.control_type)
		{
		case ControllerManager.ECONTROL_TYPE.FREE:
			if(other.tag.Equals("Ball") && _game_mode.ConditionsForShoot(this))
			{
				_on_shot_area = true;
			}
			break;
		case ControllerManager.ECONTROL_TYPE.POSSESSION:

			if(other.tag.Equals("Wall") && IsPossessing())
			{
				_ball_script.SetTrailColors(Player.EPLAYER_ID.COUNT, Player.ETEAM_ID.COUNT);
			}
			break;
		default:
			break;
		}
	}

	bool IsPossessing()
	{
		return _ball_script.IsParent (transform);
	}

	void OnTriggerExit(Collider other)
	{
		if(other.tag.Equals("Ball"))
		{
			_on_shot_area = false;
		}
	}
	
	// Update is called once per frame
	void Update () {

		if(_game_manager.game_phase != GameManager.EGAME_PHASE.MATCH || _game_manager.pause_game)
		{
			return;
		}
		CheckActions ();
		if(_possess_cooldown > 0)
		{
			_possess_cooldown -= Time.deltaTime;
		}

		if(_status == EPLAYER_STATUS.STUMBLING)
		{
			_stumble_time += Time.deltaTime;
			if(_stumble_time >= stumble_shutdown)
			{
				_status = EPLAYER_STATUS.NONE;
			}
		}
	}

	void LateUpdate()
	{
		UpdateAnimation ();
	}

	//update player animations
	void UpdateAnimation()
	{
		Vector2 vec = new Vector2(_rigid_body.velocity.x, _rigid_body.velocity.z);
		float mag = Mathf.Clamp (vec.magnitude, 0, 8f) / 10f;

		if (_status == EPLAYER_STATUS.DASHING) 
		{
			mag = 1f;
		}

		_animator.SetFloat("Forward", mag, 0.1f, Time.deltaTime);
		_animator.SetBool("OnGround", _status != EPLAYER_STATUS.JUMPING);
		_animator.SetFloat ("Jump", _rigid_body.velocity.y);
	}

	void UpdateRotationAndPosition()
	{
		if(_move_vector != Vector3.zero)
		{
			_look_dir = _move_vector;
			transform.rotation = Quaternion.LookRotation (_look_dir);
		}
		else
		{
			_look_dir = transform.forward;
		}
		_move_vector *= 0.2f;
		

		if( !IsPossessing() || _game_mode.game_mode_type != GameManager.EGAME_MODE.DODGEBALL )
		{
			_rigid_body.AddForce(_move_vector * run_force, ForceMode.Acceleration);
		}

	}

	void CheckDashingStatus()
	{
		if(_status == EPLAYER_STATUS.DASHING)
		{
			bool first_condition = _rigid_body.velocity.magnitude <= velocity_dash_stop && _input_vector != Vector2.zero;
			bool second_condition = _rigid_body.velocity.magnitude <= velocity_dash_stop_itself 
				&& _rigid_body.velocity.magnitude < _last_velocity.magnitude;
			//if player velocity is below the minimum dash velocity  then stop dashing
			if(first_condition || second_condition)
			{
				_status = EPLAYER_STATUS.NONE;
			
			}
		}
		else
		{
			//render a trail while dashing
			if (_rigid_body.velocity.magnitude < min_vel_dash_trail) 
			{
				DecayTrail();
			}
		}
	}

	//calculate if the dash trail should keep active
	void DecayTrail()
	{
		if (trail_renderer.time > 0) 
		{
			trail_renderer.time -= Time.deltaTime * decay_trail;
		}
	}

	void UpdateMoveVector()
	{
		//update left stick controls
		if (!IsAI () && _game_manager.game_phase == GameManager.EGAME_PHASE.MATCH) 
		{
			_input_vector.x = _cm.GetAxis (_controller_id, "H");
			_input_vector.y = _cm.GetAxis (_controller_id, "V");
		}

		if (_using_kinetic_control) 
		{
			_input_vector = Vector2.zero;
		}

		_move_vector = _input_vector.x * Vector3.right + _input_vector.y * Vector3.forward;
		_move_vector.Normalize ();

		if(_status != EPLAYER_STATUS.NONE)
		{
			_move_vector  = Vector3.zero;
		}
	}

	public void InputVector(Vector2 iv)
	{
		_input_vector = iv;
	}

	public void TimePressShoot(float t)
	{
		_time_pressing_shoot = t;
	}

	Vector3 GetShootProperties(ESHOOT_TYPE shot_type)
	{
		Vector3 ldir = _look_dir;
		_time_pressing_shoot = Mathf.Clamp (_time_pressing_shoot, 0, shot_charge_time);
		float time_shoot_ratio = _time_pressing_shoot / shot_charge_time;;

		switch(shot_type)
		{
		case ESHOOT_TYPE.CHIP_SHOT:
			time_shoot_ratio = 1f;
			_time_pressing_shoot = shot_charge_time;
			break;
		default:
			break;
		}


		ldir  *= Mathf.Clamp (ball_shoot_force * ( 1 - time_shoot_ratio) , ball_shoot_force * max_dampen_xz, ball_shoot_force);
		ldir.y = _time_pressing_shoot;
		ldir.y  *= Mathf.Clamp (ball_shoot_force * ( 1 - time_shoot_ratio) , ball_shoot_force * max_dampen_y, ball_shoot_force);

		return ldir;
	}



	void CheckForShoot()
	{
		bool shoot_button = _cm.GetButtonUp (_controller_id, "Shoot");
		bool chip_shot_button =  _cm.GetButtonUp (_controller_id, "Jump");

		bool pressed_shoot =  !IsAI () && (shoot_button || chip_shot_button)  || IsAI() && _ai.Shoot ();

		if((HasBall() || _on_shot_area) && pressed_shoot)
		{
			if(shoot_button)
			{
				Shoot(ESHOOT_TYPE.NORMAL);
			}
			else if(chip_shot_button)
			{
				Shoot(ESHOOT_TYPE.CHIP_SHOT);
			}
		}
		else if(HasBall () && _cm.GetButton (_controller_id, "Shoot")) 
		{
			_time_pressing_shoot += Time.deltaTime;
		}
	}

	public void Shoot(ESHOOT_TYPE shot_type = ESHOOT_TYPE.NORMAL)
	{
		_curr_kinetic_force = kinetic_force;
		_possess_cooldown = possess_cooldown_value;
		Vector3 shot_dir = GetShootProperties(shot_type);

		Vector3 ps_new_pos = _ps.transform.localPosition;
		ps_new_pos.y = _ball_rb.transform.localPosition.y;
		_ps.transform.localPosition = ps_new_pos;
		_ps.startSpeed = 5f + _rigid_body.velocity.magnitude;

		//detach the ball from the player and shoot it
		_ball_script.UnPossess();
		_ball_rb.AddForce (shot_dir, shoot_force_mode);

		//fire the shot particle system
		_ps.Play ();

		_ball_script.SetTrailColors (id, teamID);

		_time_pressing_shoot = 0f;
		_sm.PlaySound (SoundManager.ESOUNDS.SHOOT);
	}

	void CheckForDash()
	{
		bool pressed_dash = !IsAI () && _cm.GetButtonUp(_controller_id, "Dash")	||  IsAI() && _ai.Dash ();

		if( pressed_dash && _status == EPLAYER_STATUS.NONE  && !_ball_script.IsParent(transform))
		{
			_rigid_body.AddForce(transform.forward * dash_force, dash_force_mode);
			_status = EPLAYER_STATUS.DASHING;
			trail_renderer.time = 2f;
		}
	}

	void NullVelocity()
	{
		_rigid_body.velocity = Vector3.zero;
	}

	//when hit by another player, stumble in the opposite direction its facing
	void Stumble()
	{
		Vector3 stumble_dir = (- transform.forward + Vector3.up).normalized;
		NullVelocity ();
		_rigid_body.AddForce(stumble_dir * stumble_force , ForceMode.Impulse);
		_status = EPLAYER_STATUS.STUMBLING;
		_stumble_time = 0f;
	}

	void CheckForKineticControl()
	{
		//if the ball can be "force controlled", then get the value of the right stick and affect the ball with the updated values
		if(_ball_script.CanKineticControl(transform))
		{
			Vector3 k_force = KineticControlValue();

			_ball_rb.AddForce(k_force);

			_curr_kinetic_force = Mathf.Clamp (_curr_kinetic_force - Time.deltaTime * kinetic_timer, 0f, kinetic_force);
			if(Mathf.Approximately(_curr_kinetic_force, 0f))
			{
				_using_kinetic_control = false;
				_ball_script.SetTrailColors(Player.EPLAYER_ID.COUNT, Player.ETEAM_ID.COUNT);
			}
		}
		else
		{
			_using_kinetic_control = false;
		}
	}

	bool IsKeyboardController()
	{
		return _controller_id == ControllerManager.ECONTROLLER_ID.K1 || _controller_id == ControllerManager.ECONTROLLER_ID.K2;
	}

	//return the right stick values to perform kinetic control of the ball
	Vector3 KineticControlValue()
	{
		if (IsKeyboardController()) 
		{
			if(_cm.GetButton(_controller_id, "Shoot"))
			{
				float x = _cm.GetAxis (_controller_id, "H");
				float y =  _cm.GetAxis (_controller_id, "V");

				_using_kinetic_control = x != 0f || y != 0f;
				return new Vector3(x, 0f, y).normalized * _curr_kinetic_force;
			}
			else
			{
				_using_kinetic_control = false;
				return Vector3.zero;
			}
		}
		else
		{
			float x = _cm.GetAxis(_controller_id, "KineticH");
			float y = -1 * _cm.GetAxis(_controller_id, "KineticV");
			return new Vector3(x, 0f, y).normalized * _curr_kinetic_force;
		}

	}

	void CheckActions()
	{
		if(_game_manager.game_phase != GameManager.EGAME_PHASE.MATCH)
		{
			return;
		}

		CheckForKeyPress ();
		CheckForDash ();
		CheckForJump ();
		CheckForShoot ();
		CheckForKineticControl ();

		if(_cm.GetButtonUp(_controller_id, "Start"))
		{
			_game_manager.pause_game = ! _game_manager.pause_game;
		}
	}

	void CheckForKeyPress()
	{
		_cm.GetButtonDown (_controller_id, "Shoot");
		_cm.GetButtonDown (_controller_id, "Dash");
	}

	void CheckForJump()
	{
		bool pressed_button = 
			_cm.GetButtonDown (_controller_id, "Jump") ;

		bool pressed_jump = !IsAI () && pressed_button ||  IsAI() && _ai.Jump ();
			
		bool can_jump_status = _status == EPLAYER_STATUS.DASHING || _status == EPLAYER_STATUS.NONE;

		if( pressed_jump && can_jump_status  && !_ball_script.IsParent(transform))
		{
			_rigid_body.AddForce(Vector3.up * jump_force_up + _rigid_body.velocity, ForceMode.Impulse );
			_status = EPLAYER_STATUS.JUMPING;
		}
	}

	void FixedUpdate()
	{	

		if(_game_manager.game_phase != GameManager.EGAME_PHASE.MATCH)
		{
			return;
		}

		UpdateMoveVector ();

		UpdateRotationAndPosition ();
		

		CheckDashingStatus ();

		if(_status == EPLAYER_STATUS.JUMPING)
		{
			ApplyFakeGravity();
		}
		
		_last_velocity = _rigid_body.velocity;
	}

	void ApplyFakeGravity()
	{
		_rigid_body.AddForce (Vector3.up * fake_gravity + _rigid_body.velocity * jump_force_front, ForceMode.Force);
	}


	void OnCollisionEnter(Collision collision) 
	{
		if(collision.collider.tag.Equals("Field") && _status != EPLAYER_STATUS.STUMBLING)
		{
			_status = EPLAYER_STATUS.NONE;
		}
	}

	void BallCollisionBehaviour(Collider other)
	{
		switch(_cm.control_type)
		{
		case ControllerManager.ECONTROL_TYPE.FREE:
			break;
		case ControllerManager.ECONTROL_TYPE.POSSESSION:
			if(other.tag.Equals("Ball"))
			{
				_on_shot_area = true;
				if( _ball_script.CanBePossessed() 
				   && !_ball_script.IsParent(transform)
				   && _possess_cooldown <= 0)
				{
					_ball_script.Possess(transform, id);
				}
			}
			break;
		default:
			break;
		}
	}

	public void BallStolen()
	{
		_possess_cooldown = possess_cooldown_value;
		Stumble ();
	}

	public void AutoShoot()
	{
		_time_pressing_shoot = 0f;
		Shoot();
	}

	public void StartMatch()
	{
		_ball_script = _game_manager.ball;
		_ball_rb = _ball_script.GetComponent< Rigidbody >();
	}

	//ball possession at the start of the match
	public void PossessionOnInit()
	{
		Vector3 init_pos = _ball_script.ball_pos_on_possess;
		init_pos.y *= 2f;
		_ball_script.transform.localPosition = init_pos;
		_ball_script.Possess(transform, id);
		if (MasterManager.instance.controller_manager.control_type == ControllerManager.ECONTROL_TYPE.FREE) 
		{
			_ball_script.UnPossess();
			_ball_rb.constraints = RigidbodyConstraints.None;
		}
	}

	public bool IsSameTeam(ETEAM_ID team)
	{
		return teamID == team;
	}


	public bool IsAI()
	{
		return _ai != null;
	}

	//TODO:
	public void InitAI(BaseAI.AI_TYPE t)
	{
//		switch(t)
//		{
//		case BaseAI.AI_TYPE.ATTACKER:
//			gameObject.AddComponent< AttackerAI >();
//			break;
//		case BaseAI.AI_TYPE.DEFENDER:
//			gameObject.AddComponent< AttackerAI >();
//			break;
//		case BaseAI.AI_TYPE.BALANCED:
//			gameObject.AddComponent< AttackerAI >();
//			break;
//		default:
//			break;
//		}
//
//		_ai = gameObject.GetComponent< BaseAI > ();
//		_ai.AIType (t);
	}

	public BaseAI AI()
	{
		return _ai;
	}

	public ControllerManager.ECONTROLLER_ID controller
	{
		get
		{
			return _controller_id;
		}
		set
		{
			_controller_id = value;
		}
	}
}
