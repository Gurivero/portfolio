﻿using UnityEngine;
using System.Collections;

public class AttackerAI : BaseAI {

	public float dist_to_dash;

	protected override void UpdateIdle()
	{
		if (_player.HasBall ()) 
		{
			SetAIAction(AI_ACTIONS.ATTACK);
		}
		else
		{
			SetAIAction(AI_ACTIONS.PURSUE);
		}
	}

	protected override void UpdatePursue()
	{
		_ball = _game_mode.ball;

		InputVector (_ball.transform.position);

		if (_player.HasBall ()) 
		{
			SetAIAction(AI_ACTIONS.ATTACK);
		}
		else if(ConditionsForDash())
		{

		}
	}

	bool ConditionsForDash()
	{
		bool dist = Vector3.Distance (_ball.transform.position, transform.position) <= dist_to_dash;

		return dist;
	}

	GoalLine ConditionsForShoot()
	{
		RaycastHit hit;

		foreach(GoalLine g in _op_goals)
		{
			if(Physics.Raycast(new Ray(transform.position, (g.transform.position - transform.position).normalized), out hit))
			{
				if(hit.collider.tag.Equals("Goal"))
				{
					return g;
				}
			}
		}

		return null;

	}

	void SetShotProperties(GoalLine g)
	{
		InputVector (g.transform.position);
	}
	
	protected override void UpdateAttack()
	{
		GoalLine g = ConditionsForShoot ();
		if(g != null)
		{
			SetShotProperties(g);
			_shoot = true;
		}
		else
		{
			if(Vector3.Distance(transform.position, _waypoint) < 10f)
			{
				_waypoint = RandomOpGoal().transform.position;
			}
			
			InputVector (_waypoint);
		}
	}



	protected override void UpdateDefend()
	{

	}

	protected override void UpdateKinetic()
	{

	}
}
