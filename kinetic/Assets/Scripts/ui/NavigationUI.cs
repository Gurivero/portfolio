﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class NavigationUI : MonoBehaviour {

	public GameObject first_button;
	
	void OnEnable()
	{
		EventSystem.current.SetSelectedGameObject(null, null);
		EventSystem.current.SetSelectedGameObject (first_button, new BaseEventData(EventSystem.current));
	}
}
