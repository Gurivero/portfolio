﻿using UnityEngine;
using System.Collections;

public class PauseMenu : MonoBehaviour {

	[SerializeField]
	GameObject _main_pause;
	
	[SerializeField]
	GameObject _settings_pause;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnEnable()
	{
		ShowMainPause ();
	}

	public void ShowMainPause()
	{
		_main_pause.SetActive (true);
		_settings_pause.SetActive (false);
	}

	public void ShowSettings()
	{
		_main_pause.SetActive (false);
		_settings_pause.SetActive (true);
	}
}
