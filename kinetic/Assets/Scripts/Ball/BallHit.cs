﻿using UnityEngine;
using System.Collections;

public class BallHit : MonoBehaviour {

	ParticleSystem _ps;

	// Use this for initialization
	void Start () {
		_ps = GetComponent< ParticleSystem > ();
	}
	
	// Update is called once per frame
	void Update () {
		if(!_ps.IsAlive())
		{
			GameObject.Destroy(gameObject);
		}
	}
}
