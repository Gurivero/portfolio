using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BaseField : MonoBehaviour {

	public Vector3[] player_init_positions;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public List< GoalLine > GetGoals(Player.ETEAM_ID id)
	{
		List< GoalLine > gs = new List<GoalLine> ();
		GoalLine[] goals = GetComponentsInChildren< GoalLine > ();
		foreach(GoalLine g in goals)
		{
			if(g.team_id == id)
			{
				gs.Add(g);
			}
		}

		return gs;
	}
}
