Hi there!

This is my prototype called Kinetic Strikers. This prototype is a sports game where two teams face each other. The main gimmick of the game is
the possibility of controlling the ball direction after shooting it, using "kinetic control".

In this prototype only the good ol' match mode is implemented, where two players or four players can play and they have to score 10 goals. 

There is keyboard and controller support. For better user experience, use XBox 360/One controllers.

The field in this prototype has two goals for each team, a lower one which attributes 1 goal, and an upper one that attributes 3 goals.

Doing some self critic to this prototype, there are for me 2 major flaws: the code should be more optimized, and there's a disturbing lack 
of code comments.

In any case, I hope you like it, and feel free to ask any questions about it!